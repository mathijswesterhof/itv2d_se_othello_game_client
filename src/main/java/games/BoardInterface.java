package games;

public interface BoardInterface {

	/**
	 * Reset the board visually
	 */
	void reset();

	/**
	 * Update board data preceding a move
	 *
	 * @param active board should be enabled or disabled
	 */
	void updateBoardData(boolean active);

	/**
	 * Update the board with the new move
	 *
	 * @param player player that did the move
	 * @param field  field that the player updated
	 * @return successful or not
	 */
	boolean applyMoveToField(int player, int field);

	/**
	 * Run board preparations for player (show valid play options)
	 *
	 * @param player player to show the preparations for
	 */
	void preprocessFields(int player);

	/**
	 * Get score for a player
	 *
	 * @param player player to get the score for
	 * @return score as int
	 */
	int getTotalScoreFor(int player);

	/**
	 * Get the fields from the board
	 *
	 * @return Fields array
	 */
	FieldInterface[] getFields();
}
