package games.TicTacToe;

import client.ApplicationListener;
import client.FXApplication;
import client.gui.controller.GameCounterController;
import com.jfoenix.controls.JFXButton;
import games.BasePlayer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import networking.game.Game;
import networking.game.GameStore;
import util.gui.Dialog;
import util.gui.DialogBuilder;
import util.gui.GameStatsLoader;
import util.gui.Loader;
import util.registry.Registries;

public class TicTacToeController extends GameCounterController implements ApplicationListener {

	@FXML
	private StackPane root;
	@FXML
	private Label username;
	@FXML
	private AnchorPane statisticsPane;
	@FXML
	private GridPane grid;
	@FXML
	private VBox gameStats;
	private Board board;
	private TicTacToeGame ticTacToeGame;
	private DialogBuilder dialogBuilder;
	private BasePlayer player;
	private GameStatsLoader gameStatsLoader;

	/**
	 * Render the playing field.
	 *
	 * @param application FXML root to render the field in
	 * @return The Scene that has been loaded
	 */
	public static Scene loadScene(FXApplication application) {
		return Loader.loadFXML(application, "scene/Tic-tac-toe/playfield.fxml");
	}

	@Override
	protected void onLoad() {
		application.addApplicationListener(this);
		board = new Board(grid);
		dialogBuilder = new DialogBuilder();
		int turnTimeout = application.getContext().getTurnTimeoutMilliseconds();
		gameStatsLoader = new GameStatsLoader(gameStats, TicTacToeGame.getPlayerString(1),
			TicTacToeGame.getPlayerString(2), turnTimeout);
		gameStatsLoader.addNodes();
	}

	@Override
	public void onShow() {
		board.reset();
		dialogBuilder.clearDialogs();
		username.setText(application.getContext().getCurrentPlayer().getUsername());
		GameStore gameStore = application.getContext().getGameStore();
		player = application.getContext().getCurrentPlayer();
		gameStatsLoader.setAIType(application.getContext().getCurrentPlayer().getAIType());
		gameStatsLoader.startGame();

		int timeoutMilliseconds = application.getContext().getTurnTimeoutMilliseconds();
		gameStatsLoader.updateTimeoutMilliseconds(timeoutMilliseconds);
		ticTacToeGame = new TicTacToeGame(gameStore, board, player, this::exitProcedure,
			gameStatsLoader, timeoutMilliseconds);
		Game game = gameStore.getCurrentGame();
		game.setGameListener(ticTacToeGame);
	}

	@FXML
	private void registerClick(MouseEvent event) {
		Node clickedNode = event.getPickResult().getIntersectedNode();
		Integer colIndex = GridPane.getColumnIndex(clickedNode);
		Integer rowIndex = GridPane.getRowIndex(clickedNode);
		int field = (rowIndex * 3) + colIndex;
		if (board.applyMoveToField(player.getPlayerNumber(), field)) {
			ticTacToeGame.doMove(field);
		}
	}

	@FXML
	private void registerForfeit(MouseEvent event) {
		Dialog dialog = dialogBuilder.createNewDialog(
			root,
			"Forfeit",
			new Label("Are you sure you want to forfeit?"),
			"RegisterForfeit"
		);
		JFXButton[] buttons = dialog.addButtons(new String[]{"Yes, exit game", "No, keep playing"});
		buttons[0].setOnAction((e) -> {
			dialog.close();
			application.getContext().getGameStore().forfeitGame(
				application.getContext().getGameStore().getCurrentGame()
			);
		});
		buttons[1].setOnAction((e) -> dialog.close());
		dialog.show();
	}

	/**
	 * Show popup when the game has ended with the reason why the game has ended Popup is only
	 * displayed when not in tournament mode
	 *
	 * @param reason for ending the game
	 */
	private void exitProcedure(String reason) {
		if (dialogBuilder.isTournamentMode()) {
			dialogBuilder.clearDialogs();
			gameStatsLoader.stopTimer();
			application.showScene(Registries.SCENE.get("game_library"));
			return;
		}
		Dialog dialog = dialogBuilder.createNewDialog(
			root,
			"End of game",
			new Label(reason),
			"ExitProcedure"
		);
		JFXButton button = dialog.addButton("To lobby");
		dialog.setOnDialogClosed((event) -> {
			dialogBuilder.clearDialogs();
			gameStatsLoader.stopTimer();
			application.showScene(Registries.SCENE.get("game_library"));
		});
		button.setOnAction((e) -> dialog.close());
		Platform.runLater(dialog::show);
	}

	@Override
	public void onSaveAndExit() {
		gameStatsLoader.stopTimer();
	}
}
