package games.TicTacToe;

import games.BoardInterface;
import javafx.application.Platform;
import javafx.scene.layout.GridPane;

public class Board implements BoardInterface {

	private Field[] fields = new Field[9];
	private GridPane grid;

	/**
	 * Create the Playing field for Tic Tac Toe
	 *
	 * @param grid FXML grid that emulates the board
	 */
	Board(GridPane grid) {
		this.grid = grid;
		for (int field = 0; field < 9; field++) {
			fields[field] = new Field(field, grid);
		}
	}

	@Override
	public Field[] getFields() {
		return fields;
	}

	@Override
	public void reset() {
		for (Field field : fields) {
			field.resetField();
		}
		grid.setDisable(true);
	}

	@Override
	public void updateBoardData(boolean active) {
		Platform.runLater(() -> grid.setDisable(!active));
	}

	@Override
	public void preprocessFields(int player) {
	}

	@Override
	public int getTotalScoreFor(int player) {
		int score = 0;
		for (Field field : fields) {
			if (field.getPlayer() == player) {
				score++;
			}
		}

		return score;
	}

	@Override
	public boolean applyMoveToField(int player, int field) {
		if (!fields[field].isEmpty()) {
			return false;
		}
		fields[field].registerMove(player);
		return true;
	}
}
