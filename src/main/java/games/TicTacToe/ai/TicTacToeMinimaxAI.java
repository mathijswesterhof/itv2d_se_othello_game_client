package games.TicTacToe.ai;

import games.ai.MinimaxAI;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Minimax implementation for Tic Tac Toe with variable max depth.
 */
public class TicTacToeMinimaxAI implements MinimaxAI {

	private static final int EMPTY = 0;
	private static final int X = 1;
	private static final int O = 2;

	public int getBestMove(int player, int[] state, int maxDepth, int timeoutMilliseconds) {
		final int depth = getDepth(state);
		final boolean maximizing = player == X;
		int bestScore = maximizing ? -1 : 1;
		int bestMove = -1;
		if (depth == 9) {
			return new int[]{0, 2, 6, 8}[ThreadLocalRandom.current().nextInt(0, 4)];
		}

		for (int i = 0; i < state.length; i++) {
			if (state[i] == 0) {
				if (bestMove == -1) {
					bestMove = i;
				}

				state[i] = player;
				final int score = minimax(getOpponentFor(player), depth - 1, depth - maxDepth,
					state);
				state[i] = 0;
				if (maximizing) {
					if (score > bestScore) {
						bestScore = score;
						bestMove = i;
					}
				} else if (score < bestScore) {
					bestScore = score;
					bestMove = i;
				}
			}
		}

		return bestMove;
	}

	/**
	 * Minimax algorithm implementation. Gets called recursively until either the max depth has been
	 * reached, the timeout has been satisfied or the game has a winner.
	 *
	 * @param player   Specifies what player the score has to be calculated for.
	 * @param depth    Specifies the current depth of the move.
	 * @param maxDepth Specifies the maximum depth the algorithm is allowed to reach.
	 * @param state    Represents the game board.
	 * @return The best score based on the parameters.
	 */
	private int minimax(int player, int depth, int maxDepth, int[] state) {
		final boolean maximizing = player == X;
		switch (evaluate(player, depth, maxDepth, state)) {
			case DRAW:
				return 0;
			case LOSS:
				return maximizing ? -1 : 1;
			case WIN:
				return maximizing ? 1 : -1;
		}

		int bestScore = maximizing ? -1 : 1;
		for (int i = 0; i < state.length; i++) {
			if (state[i] == 0) {
				state[i] = player;
				final int score = minimax(getOpponentFor(player), depth - 1, maxDepth, state);
				state[i] = 0;
				if (maximizing) {
					bestScore = Math.max(bestScore, score);
				} else {
					bestScore = Math.min(bestScore, score);
				}
			}
		}

		return bestScore;
	}

	/**
	 * Return the total amount of empty spots on the game board.
	 *
	 * @return The total amount of empty spots on the game board.
	 */
	private int getDepth(int[] state) {
		int depth = 0;
		for (int i : state) {
			if (i == 0) {
				depth++;
			}
		}

		return depth;
	}

	/**
	 * Retrieves and organizes all win states for the current board.
	 *
	 * @param state Represents the current board state.
	 * @return All win states for Tic Tac Toe.
	 */
	private int[][] getWinStates(int[] state) {
		return new int[][]{
			//Horizontal
			new int[]{state[0], state[1], state[2]},
			new int[]{state[3], state[4], state[5]},
			new int[]{state[6], state[7], state[8]},
			//Vertical
			new int[]{state[0], state[3], state[6]},
			new int[]{state[1], state[4], state[7]},
			new int[]{state[2], state[5], state[8]},
			//Diagonal
			new int[]{state[0], state[4], state[8]},
			new int[]{state[2], state[4], state[6]}
		};
	}

	/**
	 * Score calculation for the minimax algorithm.
	 *
	 * @param player   Specifies what player the score has to be calculated for.
	 * @param depth    Specifies the current depth for the associated state.
	 * @param maxDepth Specifies the maximum depth this algorithm is allowed to reach.
	 * @param state    Represents the game board.
	 * @return The total score for the specified player with the current board state.
	 */
	private Winner evaluate(int player, int depth, int maxDepth, int[] state) {
		int opponent = getOpponentFor(player);
		int[][] winStates = getWinStates(state);
		for (int[] winState : winStates) {
			if (winState[0] == opponent && winState[1] == opponent && winState[2] == opponent) {
				return Winner.LOSS;
			}
			if (winState[0] == player && winState[1] == player && winState[2] == player) {
				return Winner.WIN;
			}
		}

		return depth < 1 || depth <= maxDepth ? Winner.DRAW : Winner.NONE;
	}

	/**
	 * Return the opponent for the specified player.
	 *
	 * @param player Specifies what player to get the opponent for.
	 * @return The specified players opponent.
	 */
	private int getOpponentFor(int player) {
		return player == 1 ? 2 : 1;
	}

	/**
	 * Represents a winner for the game.
	 */
	private enum Winner {
		NONE,
		DRAW,
		WIN,
		LOSS
	}
}
