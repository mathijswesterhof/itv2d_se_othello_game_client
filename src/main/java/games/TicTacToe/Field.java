package games.TicTacToe;

import com.jfoenix.svg.SVGGlyph;
import games.FieldInterface;
import javafx.application.Platform;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

class Field implements FieldInterface {

	private int index;
	private int player = 0;
	private SVGGlyph glyphO = new SVGGlyph(-1, "glyph_O",
		"M 1280.00,768.00 C 1280.00,698.67 1266.50,632.50 1239.50,569.50 1212.50,506.50 1176.00,452.00 1130.00,406.00 1084.00,360.00 1029.50,323.50 966.50,296.50 903.50,269.50 837.33,256.00 768.00,256.00 698.67,256.00 632.50,269.50 569.50,296.50 506.50,323.50 452.00,360.00 406.00,406.00 360.00,452.00 323.50,506.50 296.50,569.50 269.50,632.50 256.00,698.67 256.00,768.00 256.00,837.33 269.50,903.50 296.50,966.50 323.50,1029.50 360.00,1084.00 406.00,1130.00 452.00,1176.00 506.50,1212.50 569.50,1239.50 632.50,1266.50 698.67,1280.00 768.00,1280.00 837.33,1280.00 903.50,1266.50 966.50,1239.50 1029.50,1212.50 1084.00,1176.00 1130.00,1130.00 1176.00,1084.00 1212.50,1029.50 1239.50,966.50 1266.50,903.50 1280.00,837.33 1280.00,768.00 Z M 1536.00,768.00 C 1536.00,907.33 1501.67,1035.83 1433.00,1153.50 1364.33,1271.17 1271.17,1364.33 1153.50,1433.00 1035.83,1501.67 907.33,1536.00 768.00,1536.00 628.67,1536.00 500.17,1501.67 382.50,1433.00 264.83,1364.33 171.67,1271.17 103.00,1153.50 34.33,1035.83 0.00,907.33 0.00,768.00 0.00,628.67 34.33,500.17 103.00,382.50 171.67,264.83 264.83,171.67 382.50,103.00 500.17,34.33 628.67,0.00 768.00,0.00 907.33,0.00 1035.83,34.33 1153.50,103.00 1271.17,171.67 1364.33,264.83 1433.00,382.50 1501.67,500.17 1536.00,628.67 1536.00,768.00 Z",
		Color.WHITE);
	private SVGGlyph glyphX = new SVGGlyph(-1, "glyph_X",
		"M 96.00,14.00 C 96.00,14.00 82.00,0.00 82.00,0.00 82.00,0.00 48.00,34.00 48.00,34.00 48.00,34.00 14.00,0.00 14.00,0.00 14.00,0.00 0.00,14.00 0.00,14.00 0.00,14.00 34.00,48.00 34.00,48.00 34.00,48.00 0.00,82.00 0.00,82.00 0.00,82.00 14.00,96.00 14.00,96.00 14.00,96.00 48.00,62.00 48.00,62.00 48.00,62.00 82.00,96.00 82.00,96.00 82.00,96.00 96.00,82.00 96.00,82.00 96.00,82.00 62.00,48.00 62.00,48.00 62.00,48.00 96.00,14.00 96.00,14.00 Z",
		Color.WHITE);

	/**
	 * Create a field for Tic Tac Toe
	 *
	 * @param pos  position of the field on the board
	 * @param grid Grid that matches the board
	 */
	Field(int pos, GridPane grid) {
		index = pos;
		glyphO.setVisible(false);
		glyphX.setVisible(false);
		grid.getChildren().get(pos).getStyleClass().add("game-gridfield");

		Platform.runLater(() -> {
			grid.add(glyphO, pos % 3, pos / 3, 1, 1);
			grid.add(glyphX, pos % 3, pos / 3, 1, 1);
		});
	}

	@Override
	public int getPlayer() {
		return player;
	}

	@Override
	public boolean isEmpty() {
		return (player == 0);
	}

	@Override
	public void resetField() {
		player = 0;
		glyphO.setVisible(false);
		glyphX.setVisible(false);
	}

	@Override
	public void registerMove(int player) {
		this.player = player;
		SVGGlyph currentGlyph = (player == 1) ? glyphX : glyphO;
		currentGlyph.setVisible(true);
	}

	@Override
	public int getIndex() {
		return index;
	}
}
