package games.TicTacToe;

import games.BaseGame;
import games.BasePlayer;
import java.util.function.Consumer;
import networking.game.Game;
import networking.game.GameListener;
import networking.game.GameStore;
import networking.game.Move;
import networking.game.event.GameEndEvent;
import networking.game.event.GameMoveEvent;
import networking.game.event.GameTurnEvent;
import util.gui.GameStatsLoader;

public class TicTacToeGame extends BaseGame implements GameListener {

	private static final int X = 1;
	private static final int O = 2;

	private final GameStore gameStore;
	private Game game;
	private Consumer<String> exitProtocol;
	private GameStatsLoader gameStatsLoader;

	/**
	 * Start a game of Tic Tac Toe
	 *
	 * @param gameStore       store to get the current game
	 * @param board           board that currently is played on
	 * @param player          local player
	 * @param exitProtocol    what to do when the game ends
	 * @param gameStatsLoader statistics for the game
	 * @param turnTimeout     how long a turn lasts
	 */
	TicTacToeGame(GameStore gameStore, Board board, BasePlayer player,
		Consumer<String> exitProtocol, GameStatsLoader gameStatsLoader, int turnTimeout) {
		super(board, player, turnTimeout);
		this.gameStore = gameStore;
		this.exitProtocol = exitProtocol;
		this.gameStatsLoader = gameStatsLoader;
		game = gameStore.getCurrentGame();
		player.setPlayerNumber(game.isLocalTurn() ? X : O);
		gameStatsLoader.setLocalPlayer(player.getPlayerNumber());
		if (player.getOpponentNumber() == X) {
			gameStatsLoader.startTurn(player.getOpponentNumber());
		}
	}

	/**
	 * Convert the integer of a player to a meaningfull string
	 *
	 * @param player int of player
	 * @return meaningful string
	 */
	public static String getPlayerString(int player) {
		return player == X ? "X" : "O";
	}

	@Override
	public void onTurn(GameTurnEvent event) {
		game = event.getGame();
		board.updateBoardData(true);
		gameStatsLoader.startTurn(player.getPlayerNumber());
		new Thread(() -> {
			int result = player.triggerTurn(board, turnTimeout);
			if (result != -1) {
				doMove(result);
			}
		}, "Move Thread").start();
	}

	@Override
	public void onEnd(GameEndEvent event) {
		gameStatsLoader.startTurn(0, event.getPlayerOneScore(), event.getPlayerTwoScore());
		gameStatsLoader.stopTimer();
		game = event.getGame();
		String reason = (event.getEndReason() == null ?
			String.format("You %s",
				event.getGameResult().getResult()) :
			String.format("You %s, reason: %s",
				event.getGameResult().getResult(), event.getEndReason())
		);
		exitProtocol.accept(reason);
	}

	@Override
	public void onMove(GameMoveEvent event) {
		game = event.getGame();
		if (!event.getMove().getPlayer().getUsername()
			.equals(event.getGame().getLocalPlayer().getUsername())) {
			board.applyMoveToField(player.getOpponentNumber(), event.getMove().getField());
		} else {
			board.applyMoveToField(player.getPlayerNumber(), event.getMove().getField());
			gameStatsLoader.startTurn(player.getOpponentNumber());
		}
	}

	@Override
	public void doMove(int field) {
		gameStore.doMove(game, new Move(player, "", field));
		board.updateBoardData(false);
	}
}
