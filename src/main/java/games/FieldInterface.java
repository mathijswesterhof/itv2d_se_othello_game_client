package games;

public interface FieldInterface {

	/**
	 * Get the player occupying the field
	 *
	 * @return int player (or 0)
	 */
	int getPlayer();

	/**
	 * Check if the field has a player
	 *
	 * @return true if empty
	 */
	boolean isEmpty();

	/**
	 * Reset the field to start positions
	 */
	void resetField();

	/**
	 * Assign a player to the field
	 *
	 * @param player player that owns the field
	 */
	void registerMove(int player);

	/**
	 * get integer for what space it occupies on the board
	 *
	 * @return place as int
	 */
	int getIndex();
}
