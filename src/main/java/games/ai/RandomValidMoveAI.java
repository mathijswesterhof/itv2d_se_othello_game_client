package games.ai;

import games.BoardInterface;
import games.FieldInterface;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class RandomValidMoveAI implements AI {

	@Override
	public int[] getOptions(int player, BoardInterface board, int timeoutMilliseconds) {
		if (board instanceof games.Reversi.Board) {
			int[] options = ((games.Reversi.Board) board).getOptions(player);

			if (options.length < 1) {
				return new int[]{-1};
			}

			return new int[]{options[ThreadLocalRandom.current().nextInt(0, options.length)]};
		} else if (board instanceof games.TicTacToe.Board) {
			ArrayList<FieldInterface> emptyFields = new ArrayList<>();
			for (FieldInterface field : board.getFields()) {
				if (field.isEmpty()) {
					emptyFields.add(field);
				}
			}

			if (emptyFields.size() < 1) {
				return new int[]{-1};
			}

			return new int[]{emptyFields
				.get(ThreadLocalRandom.current().nextInt(0, emptyFields.size())).getIndex()};
		}

		return new int[]{-1};
	}
}
