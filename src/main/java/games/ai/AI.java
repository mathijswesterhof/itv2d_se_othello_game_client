package games.ai;

import games.BoardInterface;

/**
 * The <code>AI</code> (artificial intelligence) interface helps organise the calculation of
 * possible moves based on a board.
 */
public interface AI {

	/**
	 * Calculate an array of moves based on the parameters.
	 *
	 * @param player              Specifies what player the AI is playing as
	 * @param board               Represents the game board.
	 * @param timeoutMilliseconds Specifies the maximum amount of time this function has to run.
	 * @return An array of possible moves.
	 */
	int[] getOptions(int player, BoardInterface board, int timeoutMilliseconds);

}
