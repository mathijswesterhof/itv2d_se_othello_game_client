package games.ai;

/**
 * The <code>AIType</code> helps define the multitude of AI's available for this client.
 */
public enum AIType {
	MANUAL("Persoon"),
	RANDOMVALID("Willekeurig"),
	MINIMAX_EASY("Minimax Makkelijk"),
	MINIMAX_MEDIUM("Minimax Gemiddeld"),
	MINIMAX_HARD("Minimax Moeilijk");

	private String name;

	/**
	 * Construct an <code>AIType</code> with the associated name.
	 *
	 * @param name the associated name for this type.
	 */
	AIType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
