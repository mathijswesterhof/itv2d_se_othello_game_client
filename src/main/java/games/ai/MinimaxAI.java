package games.ai;

/**
 * The <code>MinimaxAI</code> interface helps organise minimax artificial intelligences.
 */
public interface MinimaxAI {

	/**
	 * Calculate the best move based on the specified parameters.
	 *
	 * @param player              Specifies what player the AI is playing as.
	 * @param state               Represents the game board.
	 * @param maxDepth            Specifies the maximum amount of turns allowed to look ahead.
	 * @param timeoutMilliseconds Specifies the maximum amount of time this function has to run.
	 * @return The best possible move according to the implementation.
	 */
	int getBestMove(int player, int[] state, int maxDepth, int timeoutMilliseconds);

}
