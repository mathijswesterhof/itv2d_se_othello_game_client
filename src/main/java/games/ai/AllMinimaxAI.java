package games.ai;

import games.BoardInterface;
import games.Reversi.ai.ReversiMinimaxAI;
import games.TicTacToe.ai.TicTacToeMinimaxAI;

/**
 * The <code>AllMinimaxAI</code> class implements all MinimaxAI for their associated games based on
 * a maxDepth.
 */
public abstract class AllMinimaxAI implements AI {

	private final int maxDepth;
	private final TicTacToeMinimaxAI ticTacToeMinimaxAI;
	private final ReversiMinimaxAI reversiMinimaxAI;

	/**
	 * Construct an <code>AI</code> that handles requests for moves with the specified maximum
	 * depth.
	 *
	 * @param maxDepth the maximum amount of moves this AI is allowed to look ahead.
	 */
	protected AllMinimaxAI(int maxDepth) {
		this.maxDepth = maxDepth;
		ticTacToeMinimaxAI = new TicTacToeMinimaxAI();
		reversiMinimaxAI = new ReversiMinimaxAI();
	}

	@Override
	public int[] getOptions(int player, BoardInterface board, int timeoutMilliseconds) {
		int[] state = new int[board.getFields().length];
		for (int i = 0; i < state.length; i++) {
			state[i] = board.getFields()[i].getPlayer();
		}

		if (board instanceof games.TicTacToe.Board) {
			return new int[]{
				ticTacToeMinimaxAI.getBestMove(player, state, maxDepth, timeoutMilliseconds)};
		}
		if (board instanceof games.Reversi.Board) {
			return new int[]{
				reversiMinimaxAI.getBestMove(player, state, maxDepth, timeoutMilliseconds)};
		}

		return new int[]{-1};
	}

}
