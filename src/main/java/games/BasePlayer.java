package games;

import games.ai.AI;
import games.ai.AIType;
import networking.player.Player;
import util.registry.Registries;

public class BasePlayer extends Player {

	private int playerNumber;
	private AI ai;
	private AIType aiType = AIType.MANUAL;

	/**
	 * Initializes Base player that extends the API player
	 *
	 * @param username name of the player
	 * @param remote   indicate if it is a remote player
	 */
	public BasePlayer(String username, boolean remote) {
		super(username, remote);
		playerNumber = 0;
	}

	/**
	 * Get the number of the player (player 1 or 2)
	 *
	 * @return playerNumber
	 */
	public int getPlayerNumber() {
		return playerNumber;
	}

	/**
	 * Set the number of the player (player 1 or 2)
	 *
	 * @param playerNumber playerNumber
	 */
	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}

	/**
	 * Get the opponent number for this player (1 -> 2 & 2 -> 1)
	 *
	 * @return opponent int
	 */
	public int getOpponentNumber() {
		return (playerNumber == 1) ? 2 : 1;
	}

	/**
	 * Set the aiType
	 *
	 * @param aiType Type of player you are
	 */
	public void setAI(AIType aiType) {
		this.aiType = aiType;
		this.ai = Registries.AI.get(aiType.toString());
	}

	/**
	 * Get the aiType
	 *
	 * @return AIType
	 */
	public AIType getAIType() {
		return aiType;
	}

	/**
	 * Get all board options and formulate a response when turn is triggered
	 *
	 * @param board       current playing field
	 * @param turnTimeout time allowed to think about a move
	 * @return int field on which to place a move
	 */
	public int triggerTurn(BoardInterface board, int turnTimeout) {
		int turn = -1;
		if (aiType != AIType.MANUAL && ai != null) {
			int[] options = ai.getOptions(getPlayerNumber(), board, turnTimeout - 1500);
			if (options.length > 0) {
				return options[0];
			}
		} else {
			board.preprocessFields(getPlayerNumber());
		}

		return turn;
	}
}
