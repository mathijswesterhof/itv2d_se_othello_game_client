package games.Reversi;

import games.BaseGame;
import games.BasePlayer;
import java.util.function.Consumer;
import networking.game.Game;
import networking.game.GameListener;
import networking.game.GameStore;
import networking.game.Move;
import networking.game.event.GameEndEvent;
import networking.game.event.GameMoveEvent;
import networking.game.event.GameTurnEvent;
import util.gui.GameStatsLoader;

public class ReversiGame extends BaseGame implements GameListener {

	public static final int BLACK = 1;
	public static final int WHITE = 2;
	private final GameStore gameStore;
	private Game game;
	private Consumer<String> exitProtocol;
	private GameStatsLoader gameStatsLoader;
	private volatile boolean lastMoveWasLocal = false;

	ReversiGame(GameStore gameStore, Board board, BasePlayer player,
		Consumer<String> exitProtocol, GameStatsLoader gameStatsLoader, int turnTimeout) {
		super(board, player, turnTimeout);
		this.gameStore = gameStore;
		this.exitProtocol = exitProtocol;
		this.gameStatsLoader = gameStatsLoader;
		game = gameStore.getCurrentGame();
		player.setPlayerNumber(game.isLocalTurn() ? BLACK : WHITE);
		gameStatsLoader.setLocalPlayer(player.getPlayerNumber());
		if (player.getOpponentNumber() == BLACK) {
			gameStatsLoader.startTurn(player.getOpponentNumber(), board.getTotalScoreFor(BLACK),
				board.getTotalScoreFor(WHITE));
		}
	}

	public static String getPlayerString(int player) {
		return player == BLACK ? "Zwart" : "Wit";
	}

	@Override
	public void onTurn(GameTurnEvent event) {
		if (lastMoveWasLocal) {
			gameStatsLoader.showCustomMessage("EXTRA BEURT!", "reversi-extra-turn-label");
		}
		game = event.getGame();
		board.updateBoardData(true);
		gameStatsLoader.startTurn(player.getPlayerNumber(), board.getTotalScoreFor(BLACK),
			board.getTotalScoreFor(WHITE));
		new Thread(() -> {
			int result = player.triggerTurn(board, turnTimeout);
			if (result != -1) {
				doMove(result);
			}
		}, "Move Thread").start();
		lastMoveWasLocal = true;
	}

	@Override
	public void onEnd(GameEndEvent event) {
		gameStatsLoader.startTurn(0, event.getPlayerOneScore(), event.getPlayerTwoScore());
		gameStatsLoader.stopTimer();
		game = event.getGame();
		String reason = (event.getEndReason() == null ?
			String.format("You %s",
				event.getGameResult().getResult()) :
			String.format("You %s, reason: %s",
				event.getGameResult().getResult(), event.getEndReason())
		);
		exitProtocol.accept(reason);
	}

	@Override
	public void onMove(GameMoveEvent event) {
		game = event.getGame();
		if (!event.getMove().getPlayer().getUsername()
			.equals(event.getGame().getLocalPlayer().getUsername())) {
			board.applyMoveToField(player.getOpponentNumber(), event.getMove().getField());
			lastMoveWasLocal = false;
			gameStatsLoader.hideCustomMessage();
		} else {
			board.applyMoveToField(player.getPlayerNumber(), event.getMove().getField());
			gameStatsLoader.startTurn(player.getOpponentNumber(), board.getTotalScoreFor(BLACK),
				board.getTotalScoreFor(WHITE));
		}
	}

	@Override
	public void doMove(int field) {
		gameStore.doMove(game, new Move(player, "", field));
		board.updateBoardData(false);
	}
}
