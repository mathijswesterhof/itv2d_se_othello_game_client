package games.Reversi;

import games.BoardInterface;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javafx.scene.layout.GridPane;

public class Board implements BoardInterface {

	private Field[] fields = new Field[64];
	private Field[][] H_fields = new Field[8][8];
	private GridPane grid;

	/**
	 * Create the Playing field for Reversi
	 *
	 * @param grid FXML grid that emulates the board
	 */
	Board(GridPane grid) {
		this.grid = grid;
		for (int pos = 0; pos < 64; pos++) {
			fields[pos] = new Field(pos, grid);
			H_fields[pos % 8][pos / 8] = fields[pos];
		}
	}

	/**
	 * Transform a list of integers to a unique array
	 *
	 * @param integers list of duplicates
	 * @return array of unique
	 */
	private static int[] convertIntegers(List<Integer> integers) {
		Set<Integer> set = new LinkedHashSet<>(integers);
		integers.clear();
		integers.addAll(set);
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next();
		}
		return ret;
	}

	@Override
	public Field[] getFields() {
		return fields;
	}

	@Override
	public void reset() {
		for (Field field : fields) {
			field.resetField();
		}
		fields[27].registerMove(2);
		fields[36].registerMove(2);
		fields[28].registerMove(1);
		fields[35].registerMove(1);
		grid.setDisable(true);
	}

	@Override
	public void updateBoardData(boolean active) {
		for (Field field : fields) {
			field.resetHighlight();
		}
		grid.setDisable(!active);
	}

	@Override
	public void preprocessFields(int player) {
		int[] options = getOptions(player);
		highlightFields(options);
	}

	@Override
	public boolean applyMoveToField(int player, int field) {
		if (!fields[field].isEmpty()) {
			return false;
		}
		int x = field % 8;
		int y = field / 8;
		ArrayList<Integer> mem = new ArrayList<>();
		mem.add(field);
		for (int xMod = -1; xMod < 2; xMod++) {
			for (int yMod = -1; yMod < 2; yMod++) {
				if (xMod == 0 && yMod == 0) {
					continue;
				}
				checkNeighboringStonesAsMove(x, y, xMod, yMod, player, mem);
			}
		}
		for (int m : mem) {
			fields[m].registerMove(player);
		}
		return true;
	}

	@Override
	public int getTotalScoreFor(int player) {
		int score = 0;
		for (Field field : fields) {
			if (field.getPlayer() == player) {
				score++;
			}
		}

		return score;
	}

	/**
	 * Check for stones that should be swapped after move
	 *
	 * @param x      position on board
	 * @param y      position on board
	 * @param xMod   direction to modify
	 * @param yMod   direction to modify
	 * @param player player to look for
	 * @param mem    previous registered moves
	 * @return true if stone should be swapped
	 */
	private boolean checkNeighboringStonesAsMove(
		int x, int y,
		int xMod, int yMod,
		int player, ArrayList<Integer> mem
	) {
		x = x + xMod;
		y = y + yMod;
		if (y >= 0 && y < 8 && x >= 0 && x < 8 && !H_fields[x][y].isEmpty()) {
			if (H_fields[x][y].getPlayer() == player) {
				return true;
			} else if (checkNeighboringStonesAsMove(x, y, xMod, yMod, player, mem)) {
				mem.add((y * 8) + x);
				return true;
			}
		}
		return false;
	}

	/**
	 * Highlight fields
	 *
	 * @param positions fields to be highlighted
	 */
	private void highlightFields(int[] positions) {
		for (int position : positions) {
			fields[position].highlight();
		}
	}

	/**
	 * Get options to place a stone for the current player
	 *
	 * @param player current active player
	 * @return list of valid fields to place a move
	 */
	public int[] getOptions(int player) {
		ArrayList<Integer> results = new ArrayList<>();
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (H_fields[x][y].getPlayer() == player) {
					for (int xMod = -1; xMod < 2; xMod++) {
						for (int yMod = -1; yMod < 2; yMod++) {
							if (xMod == 0 && yMod == 0) {
								continue;
							}
							checkFreeNeighbors(x, y, xMod, yMod, player, true, results);
						}
					}
				}
			}
		}
		return convertIntegers(results);
	}

	/**
	 * Check if a neigboring field is free
	 *
	 * @param x          position on board
	 * @param y          position on board
	 * @param xMod       direction to look for
	 * @param yMod       direction to look for
	 * @param player     player to look for
	 * @param firstRound in recursive loop
	 * @param mem        previous found fields
	 */
	private void checkFreeNeighbors(
		int x, int y,
		int xMod, int yMod,
		int player, boolean firstRound, ArrayList<Integer> mem
	) {
		x = x + xMod;
		y = y + yMod;
		if (y >= 0 && y < 8 && x >= 0 && x < 8) {
			if (H_fields[x][y].isEmpty() && !firstRound) {
				mem.add((y * 8) + x);
			} else if (!H_fields[x][y].isEmpty() && H_fields[x][y].getPlayer() != player) {
				checkFreeNeighbors(x, y, xMod, yMod, player, false, mem);
			}
		}
	}
}
