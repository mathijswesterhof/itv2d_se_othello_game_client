package games.Reversi;

import games.FieldInterface;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Field implements FieldInterface {

	private int index;
	private int player = 0;
	private Circle stone;
	private Region region;

	/**
	 * Create a field for Reversi
	 *
	 * @param pos  position of the field on the board
	 * @param grid Grid that matches the board
	 */
	Field(int pos, GridPane grid) {
		index = pos;
		region = createRegion(grid, pos / 8, pos % 8);
		stone = createCircle(grid, pos / 8, pos % 8);
	}

	/**
	 * Add style class to this field to highlight it
	 */
	void highlight() {
		Platform.runLater(() -> region.getStyleClass().add("hint"));
	}

	/**
	 * Remove the styleClass
	 */
	void resetHighlight() {
		region.getStyleClass().remove("hint");
	}

	@Override
	public int getPlayer() {
		return player;
	}

	@Override
	public boolean isEmpty() {
		return (player == 0);
	}

	@Override
	public void resetField() {
		player = 0;
		stone.setVisible(false);
		resetHighlight();
	}

	@Override
	public void registerMove(int player) {
		this.player = player;
		Color c = (player == 1) ? Color.BLACK : Color.WHITE;
		Platform.runLater(() -> {
			stone.setFill(c);
			if (!stone.isVisible()) {
				stone.setVisible(true);
			}
		});
	}

	/**
	 * Create a Circle object that acts as a Reversi stone
	 *
	 * @param grid Grid to place it onto
	 * @param x    position in the grid
	 * @param y    position in the grid
	 * @return Circle
	 */
	private Circle createCircle(GridPane grid, int x, int y) {
		Circle circle = new Circle(30);
		circle.setEffect(new DropShadow(5, 3, 3, Color.BLACK));
		circle.setVisible(false);
		GridPane.setHalignment(circle, HPos.CENTER);
		GridPane.setValignment(circle, VPos.CENTER);
		grid.add(circle, y, x);
		return circle;
	}

	/**
	 * Create a Region object that acts as a background highlighter
	 *
	 * @param grid Grid to place it onto
	 * @param x    position in the grid
	 * @param y    position in the grid
	 * @return Region
	 */
	private Region createRegion(GridPane grid, int x, int y) {
		Region r = new Region();
		r.getStyleClass().add("game-gridfield");
		grid.add(r, y, x);
		return r;
	}

	@Override
	public int getIndex() {
		return index;
	}
}
