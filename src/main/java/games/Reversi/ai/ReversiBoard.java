package games.Reversi.ai;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a reversi game board. This game board is optimized for use by artificial
 * intelligences.
 */
public class ReversiBoard {

	private static final int EMPTY = 0;
	private static final int BLACK = 1;
	private static final int WHITE = 2;

	private final int[] fields;
	private final int[][] H_fields;

	/**
	 * Construct a reversi game board from the specified state.
	 *
	 * @param state Represents a reversi game board.
	 */
	public ReversiBoard(int[] state) {
		fields = new int[64];
		H_fields = new int[8][8];
		for (int pos = 0; pos < 64; pos++) {
			fields[pos] = state[pos];
			H_fields[pos % 8][pos / 8] = state[pos];
		}
	}

	/**
	 * Efficiently convert a <code>List&lt;Integer&gt;</code> to a primitive <code>int[]</code>.
	 *
	 * @param integers Specifies what list to convert.
	 * @return An array of int.
	 */
	public static int[] convertIntegers(List<Integer> integers) {
		Set<Integer> set = new LinkedHashSet<>(integers);
		integers.clear();
		integers.addAll(set);
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next();
		}
		return ret;
	}

	/**
	 * Return the boards fields.
	 *
	 * @return The fields associated with this board.
	 */
	public int[] getFields() {
		return fields;
	}

	/**
	 * Create a deep copy of this board for use in recursion.
	 *
	 * @return A deep copy of this board.
	 */
	public ReversiBoard getDeepCopy() {
		return new ReversiBoard(fields.clone());
	}

	private boolean checkNeighboringStonesAsMove(
		int x, int y,
		int xMod, int yMod,
		int player, ArrayList<Integer> mem
	) {
		x = x + xMod;
		y = y + yMod;
		if (y >= 0 && y < 8 && x >= 0 && x < 8 && H_fields[x][y] != EMPTY) {
			if (H_fields[x][y] == player) {
				return true;
			} else if (checkNeighboringStonesAsMove(x, y, xMod, yMod, player, mem)) {
				mem.add((y * 8) + x);
				return true;
			}
		}
		return false;
	}

	/**
	 * Apply a move to the game board.
	 *
	 * @return True if the move has been applied, False if there was an issue.
	 */
	public boolean applyMoveToField(int player, int field) {
		if (fields[field] != EMPTY) {
			return false;
		}

		int x = field % 8;
		int y = field / 8;
		ArrayList<Integer> mem = new ArrayList<>();
		mem.add(field);
		for (int xMod = -1; xMod < 2; xMod++) {
			for (int yMod = -1; yMod < 2; yMod++) {
				if (xMod == 0 && yMod == 0) {
					continue;
				}
				checkNeighboringStonesAsMove(x, y, xMod, yMod, player, mem);
			}
		}
		for (int m : mem) {
			fields[m] = player;
		}
		return true;
	}

	private void checkFreeNeighbors(
		int x, int y,
		int xMod, int yMod,
		int player, boolean firstRound, ArrayList<Integer> mem
	) {
		x = x + xMod;
		y = y + yMod;
		if (y >= 0 && y < 8 && x >= 0 && x < 8) {
			if (H_fields[x][y] == EMPTY && !firstRound) {
				mem.add((y * 8) + x);
			} else if (H_fields[x][y] != EMPTY && H_fields[x][y] != player) {
				checkFreeNeighbors(x, y, xMod, yMod, player, false, mem);
			}
		}
	}

	/**
	 * Return all valid moves for the specified player.
	 *
	 * @param player Specifies what player to find the valid moves for.
	 * @return All possible moves for the specified player.
	 */
	public int[] getOptions(int player) {
		ArrayList<Integer> results = new ArrayList<>();
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (H_fields[x][y] == player) {
					for (int xMod = -1; xMod < 2; xMod++) {
						for (int yMod = -1; yMod < 2; yMod++) {
							if (xMod == 0 && yMod == 0) {
								continue;
							}
							checkFreeNeighbors(x, y, xMod, yMod, player, true, results);
						}
					}
				}
			}
		}
		return convertIntegers(results);
	}

	/**
	 * Calculate the total score for the specified player.
	 *
	 * @param player Specifies what player to calculate the total score for.
	 * @return Total score for the specified player.
	 */
	public int getTotalScoreFor(int player) {
		int score = 0;
		for (int field : fields) {
			if (field == player) {
				score++;
			}
		}

		return score;
	}

	/**
	 * Return the winner for the game board,
	 *
	 * @return The winner for the current game board.
	 */
	public Winner getWinner() {
		if (getOptions(BLACK).length < 1 && getOptions(WHITE).length < 1) {
			final int blackScore = getTotalScoreFor(BLACK);
			final int whiteScore = getTotalScoreFor(WHITE);
			if (blackScore == whiteScore) {
				return Winner.DRAW;
			}
			if (blackScore > whiteScore) {
				return Winner.BLACK;
			}

			return Winner.WHITE;
		}
		return Winner.NONE;
	}

	/**
	 * Return the total amount of empty spots on the game board.
	 *
	 * @return The total amount of empty spots on the game board.
	 */
	public int getDepth() {
		int depth = 0;
		for (int field : fields) {
			if (field != EMPTY) {
				depth++;
			}
		}

		return depth;
	}

	/**
	 * Represents a winner for the game.
	 */
	public enum Winner {
		NONE,
		DRAW,
		BLACK,
		WHITE
	}
}
