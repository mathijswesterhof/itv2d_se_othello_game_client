package games.Reversi.ai;

/**
 * This class represents a move with a score for the reversi AI.
 */
public class ScoredMove {

	public final int move;
	public volatile int score;

	/**
	 * Construct a move that can be scored.
	 *
	 * @param score Specifies what score is associated with this move.
	 * @param move  Specifies what move is being represented.
	 */
	public ScoredMove(int score, int move) {
		this.score = score;
		this.move = move;
	}

}
