package games.Reversi.ai;

import games.ai.MinimaxAI;

/**
 * Minimax implementation for reversi with timeout, variable max iteration depth and field weights.
 */
public class ReversiMinimaxAI implements MinimaxAI {

	private static final int EMPTY = 0;
	private static final int BLACK = 1;
	private static final int WHITE = 2;
	private static final int HIGH_PRIO = 10;
	private static final int MEDIUM_PRIO = 8;
	private static final int DEFAULT_PRIO = 1;
	private static final int LOWER_PRIO = -8;
	private static final int LOWEST_PRIO = -15;
	private static final int[] FIELD_WEIGHTS = new int[]{
		HIGH_PRIO, LOWEST_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, LOWEST_PRIO,
		HIGH_PRIO,
		LOWEST_PRIO, LOWEST_PRIO, LOWER_PRIO, LOWER_PRIO, LOWER_PRIO, LOWER_PRIO, LOWEST_PRIO,
		LOWEST_PRIO,
		MEDIUM_PRIO, LOWER_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, LOWER_PRIO,
		MEDIUM_PRIO,
		MEDIUM_PRIO, LOWER_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, LOWER_PRIO,
		MEDIUM_PRIO,
		MEDIUM_PRIO, LOWER_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, LOWER_PRIO,
		MEDIUM_PRIO,
		MEDIUM_PRIO, LOWER_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, DEFAULT_PRIO, LOWER_PRIO,
		MEDIUM_PRIO,
		LOWEST_PRIO, LOWEST_PRIO, LOWER_PRIO, LOWER_PRIO, LOWER_PRIO, LOWER_PRIO, LOWEST_PRIO,
		LOWEST_PRIO,
		HIGH_PRIO, LOWEST_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, MEDIUM_PRIO, LOWEST_PRIO,
		HIGH_PRIO,
	};

	public int getBestMove(int player, int[] state, int maxDepth, int timeoutMilliseconds) {
		ReversiBoard board = new ReversiBoard(state);
		final int[] options = board.getOptions(player);
		if (options.length < 1) {
			return -1;
		}

		final boolean maximizing = player == BLACK;
		final int depth = board.getDepth();
		final Thread[] threads = new Thread[options.length];
		final ScoredMove[] moves = new ScoredMove[options.length];
		for (int i = 0; i < options.length; i++) {
			final int index = i;
			final int option = options[i];
			final ReversiBoard newBoard = board.getDeepCopy();
			newBoard.applyMoveToField(player, option);
			threads[i] = new Thread(() -> {
				moves[index] = new ScoredMove(maximizing ? Integer.MIN_VALUE : Integer.MAX_VALUE,
					option);
				moves[index].score = minimax(getOpponentFor(player), depth - 1, depth - maxDepth,
					newBoard);
			}, "MINIMAX Thread");
			threads[i].start();
		}

		final long endTime = System.currentTimeMillis() + timeoutMilliseconds;
		while (System.currentTimeMillis() < endTime) {
			boolean alive = false;
			for (Thread thread : threads) {
				if (thread.isAlive()) {
					alive = true;
					break;
				}
			}

			if (!alive) {
				break;
			}

			try {
				Thread.sleep(timeoutMilliseconds / 100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}

		boolean alive = true;
		while (alive) {
			boolean a = false;
			for (Thread thread : threads) {
				if (thread.isAlive()) {
					thread.interrupt();
					a = true;
					break;
				}
			}

			alive = a;
		}

		int bestScore = moves[0].score;
		int bestMove = moves[0].move;
		for (ScoredMove move : moves) {
			if (maximizing) {
				if (move.score > bestScore) {
					bestScore = move.score;
					bestMove = move.move;
				}
			} else if (move.score < bestScore) {
				bestScore = move.score;
				bestMove = move.move;
			}
		}

		return bestMove;
	}

	/**
	 * Minimax algorithm implementation. Gets called recursively until either the max depth has been
	 * reached, the timeout has been satisfied or the game has a winner.
	 *
	 * @param player   Specifies what player the score has to be calculated for.
	 * @param depth    Specifies the current depth of the move.
	 * @param maxDepth Specifies the maximum depth the algorithm is allowed to reach.
	 * @param board    Represents the game board.
	 * @return The best score based on the parameters.
	 */
	private int minimax(int player, int depth, int maxDepth, ReversiBoard board) {
		if (Thread.currentThread().isInterrupted()) {
			return evaluate(player, board);
		}

		final boolean maximizing = player == BLACK;
		final int[] options = board.getOptions(player);
		if (depth < 1 || depth <= maxDepth || options.length < 1) {
			switch (board.getWinner()) {
				case DRAW:
					return 0;
				case BLACK:
					return maximizing ? Integer.MAX_VALUE : Integer.MIN_VALUE;
				case WHITE:
					return maximizing ? Integer.MIN_VALUE : Integer.MAX_VALUE;
			}

			return evaluate(player, board);
		}

		int bestScore = maximizing ? Integer.MIN_VALUE : Integer.MAX_VALUE;
		for (final int option : options) {
			final ReversiBoard newBoard = board.getDeepCopy();
			newBoard.applyMoveToField(player, option);
			final int score = minimax(getOpponentFor(player), depth - 1, maxDepth, newBoard);
			if (maximizing) {
				bestScore = Math.max(bestScore, score);
			} else {
				bestScore = Math.min(bestScore, score);
			}
		}
		return bestScore;
	}

	/**
	 * Score calculation for the minimax algorithm.
	 *
	 * @param player Specifies what player the score has to be calculated for.
	 * @param board  Represents the game board.
	 * @return The total score for the specified player with the current board state.
	 */
	private int evaluate(int player, ReversiBoard board) {
		final boolean maximizing = player == BLACK;
		final int[] fields = board.getFields();
		int weightedScore = 0;
		for (int i = 0; i < fields.length; i++) {
			final int field = fields[i];
			if (field == player) {
				if (maximizing) {
					weightedScore += FIELD_WEIGHTS[i];
				} else {
					weightedScore -= FIELD_WEIGHTS[i];
				}
			}
		}

		return weightedScore;
	}

	/**
	 * Return the opponent for the specified player.
	 *
	 * @param player Specifies what player to get the opponent for.
	 * @return The specified players opponent.
	 */
	private int getOpponentFor(int player) {
		return player == BLACK ? WHITE : BLACK;
	}
}
