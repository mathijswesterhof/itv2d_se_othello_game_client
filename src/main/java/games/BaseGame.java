package games;

public abstract class BaseGame {

	public BasePlayer player;
	public BoardInterface board;
	protected int turnTimeout;

	/**
	 * Create base components for each game
	 *
	 * @param board       board instance
	 * @param player      local player
	 * @param turnTimeout time to think about a move
	 */
	public BaseGame(BoardInterface board, BasePlayer player, int turnTimeout) {
		this.player = player;
		this.board = board;
		this.turnTimeout = turnTimeout;
	}

	/**
	 * Get the playing field
	 *
	 * @return Board
	 */
	public BoardInterface getBoard() {
		return board;
	}

	/**
	 * Trigger a move on the specified field
	 *
	 * @param field field number on the board
	 */
	public abstract void doMove(int field);
}
