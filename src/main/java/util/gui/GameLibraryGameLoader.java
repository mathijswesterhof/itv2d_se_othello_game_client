package util.gui;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import networking.game.GameType;

public class GameLibraryGameLoader {

	private ArrayList<Node[]> gameList;
	private HBox border;

	/**
	 * Hold a list of games inside the border
	 *
	 * @param border border to display game objects in
	 */
	public GameLibraryGameLoader(HBox border) {
		this.border = border;
		gameList = new ArrayList<>();
	}

	/**
	 * Add Game to game list
	 *
	 * @param name     GameType object for game
	 * @param describe description of the game
	 */
	public void addNode(GameType name, String describe) {
		Node[] g = gameBuilder(name, describe);
		border.getChildren().add(g[0]);
		gameList.add(g);
	}

	/**
	 * Set action for lobby buttons in list of games
	 *
	 * @param value Action executed upon event
	 */
	public void setLobby(EventHandler<ActionEvent> value) {
		for (Node[] n : gameList) {
			((JFXButton) n[1]).setOnAction(value);
		}
	}

	/**
	 * Set action for quickplay buttons in list of games
	 *
	 * @param value Action executed upon event
	 */
	public void setQuickPlay(EventHandler<ActionEvent> value) {
		for (Node[] n : gameList) {
			((JFXButton) n[2]).setOnAction(value);
		}
	}

	/**
	 * Create a game pane for a game
	 *
	 * @param gameType Gametype object
	 * @param describe descripton of the game
	 * @return FXML node
	 */
	private Node[] gameBuilder(GameType gameType, String describe) {
		ImageView icon = new ImageView();
		icon.setFitHeight(32);
		icon.setFitWidth(32);
		URL u = getClass().getResource(String.format("/scene/%s/icon.png", gameType.getTag()));
		if (u != null) {
			icon.setImage(new Image(String.format("/scene/%s/icon.png", gameType.getTag())));
		} else {
			icon.setImage(new Image("/image/icon_gc.png"));
		}
		icon.setPreserveRatio(true);
		HBox.setMargin(icon, new Insets(0, 10, 0, 0));
		Label gameName = new Label();
		gameName.setText(gameType.getTag());
		gameName.getStyleClass().addAll("default-label", "text-h2");
		HBox header = new HBox();
		header.setPadding(new Insets(10, 10, 10, 10));
		header.getChildren().addAll(icon, gameName);

		HBox teaser = new HBox();
		teaser.setPrefSize(400, 300);
		teaser.getStyleClass().add("game-lobby-Box-teaser");

		Label description = new Label();
		description.setWrapText(true);
		description.getStyleClass().addAll("default-label", "text-h3");
		description.setText(describe);
		HBox context = new HBox();
		context.setPadding(new Insets(10, 10, 10, 10));
		context.setPrefWidth(200);
		context.getChildren().add(description);

		JFXButton lobby = new JFXButton();
		lobby.getStyleClass().addAll("borderButton", "text-h3");
		lobby.setText("Lobby");
		lobby.getProperties().put("game", gameType);
		AnchorPane.setTopAnchor(lobby, 0d);
		AnchorPane.setRightAnchor(lobby, 125d);
		JFXButton quick = new JFXButton();
		quick.getStyleClass().addAll("borderButton", "text-h3");
		quick.setText("Snel spelen");
		quick.getProperties().put("game", gameType);
		AnchorPane.setTopAnchor(quick, 0d);
		AnchorPane.setRightAnchor(quick, 0d);
		AnchorPane controls = new AnchorPane();
		controls.setPadding(new Insets(10, 10, 10, 10));
		controls.getChildren().addAll(lobby, quick);

		VBox outside = new VBox();
		outside.getStyleClass().add("game-lobby-Box");
		HBox.setMargin(outside, new Insets(0, 60, 0, 0));
		outside.getChildren().addAll(header, teaser, context, controls);

		return new Node[]{outside, lobby, quick};
	}
}
