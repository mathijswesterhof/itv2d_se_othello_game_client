package util.gui;

import client.FXApplication;
import client.gui.controller.GameCounterController;
import client.gui.scene.ErrorScene;
import client.gui.scene.GameCounterScene;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import lang.NoSuchResourceException;

public class Loader {

	/**
	 * Load image from resources
	 *
	 * @param resourcePath path to resource
	 * @return resource
	 */
	public static Image loadImage(String resourcePath) {
		try {
			InputStream stream = Loader.class.getClassLoader().getResourceAsStream(resourcePath);
			if (stream == null) {
				throw new NoSuchResourceException();
			}

			return new Image(stream);
		} catch (NoSuchResourceException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Load fxml from resource
	 *
	 * @param application  fxml container
	 * @param resourcePath path to resource
	 * @return loaded fxml resource
	 */
	public static Scene loadFXML(FXApplication application, String resourcePath) {
		return loadFXML(application, resourcePath, null);
	}

	/**
	 * Load fxml from resource with override controller
	 *
	 * @param application     fxml container
	 * @param resourcePath    path to resource
	 * @param forceController controller to be used
	 * @return loaded fxml resource
	 */
	public static Scene loadFXML(FXApplication application, String resourcePath,
		Object forceController) {
		try {
			URL url = Loader.class.getClassLoader().getResource(resourcePath);
			if (url == null) {
				throw new NoSuchResourceException();
			}

			FXMLLoader loader = new FXMLLoader(url);
			if (forceController != null) {
				loader.setController(forceController);
			}

			Parent parent = loader.load();
			Object controller = loader.getController();
			GameCounterController counterController = null;
			if (controller != null) {
				if (controller instanceof GameCounterController) {
					counterController = (GameCounterController) controller;
					counterController.setApplication(application);
				}
			}

			return new GameCounterScene(parent, counterController);
		} catch (NoSuchResourceException | IOException e) {
			e.printStackTrace();
			return ErrorScene.fromException(e);
		}
	}
}
