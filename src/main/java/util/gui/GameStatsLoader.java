package util.gui;

import games.ai.AIType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class GameStatsLoader {

	private final SimpleDateFormat dateFormat;
	private final VBox border;
	private final String playerOne;
	private final String playerTwo;
	private int timeoutMilliseconds;
	private volatile long turnEndTime;
	private volatile long gameStartTime;
	private Label aiType;
	private Label localPlayer;
	private Label playerTurn;
	private Label playerOneScore;
	private Label playerTwoScore;
	private Label moveTimer;
	private Label totalTimer;
	private Label customMessage;
	private Thread timerThread;
	
	/**
	 * Initialize the statistics pane
	 *
	 * @param border              containment in which the statistics are shown
	 * @param playerOne           first player
	 * @param playerTwo           second player
	 * @param timeoutMilliseconds move timeout time
	 */
	public GameStatsLoader(VBox border, String playerOne, String playerTwo,
		int timeoutMilliseconds) {
		this.dateFormat = new SimpleDateFormat("HH:mm:ss");
		this.dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		this.border = border;
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.timeoutMilliseconds = timeoutMilliseconds;
	}

	/**
	 * Set all the FXML Nodes for the statistics pane
	 */
	public void addNodes() {
		aiType = new Label();
		localPlayer = new Label();
		playerTurn = new Label();
		playerOneScore = new Label();
		playerTwoScore = new Label();
		moveTimer = new Label();
		totalTimer = new Label();
		customMessage = new Label();
		aiType.getStyleClass().add("game-stats-label");
		localPlayer.getStyleClass().add("game-stats-label");
		playerTurn.getStyleClass().add("game-stats-label");
		playerOneScore.getStyleClass().add("game-stats-label");
		playerTwoScore.getStyleClass().add("game-stats-label");
		moveTimer.getStyleClass().add("game-stats-label");
		totalTimer.getStyleClass().add("game-stats-label");
		customMessage.getStyleClass().add("game-stats-label");
		Insets labelMargin = new Insets(0, 0, 10, 0);
		VBox.setMargin(aiType, labelMargin);
		VBox.setMargin(localPlayer, labelMargin);
		VBox.setMargin(playerTurn, labelMargin);
		VBox.setMargin(playerOneScore, labelMargin);
		VBox.setMargin(playerTwoScore, labelMargin);
		VBox.setMargin(moveTimer, labelMargin);
		VBox.setMargin(totalTimer, labelMargin);
		VBox.setMargin(customMessage, labelMargin);
		playerOneScore.managedProperty().bind(playerOneScore.visibleProperty());
		playerTwoScore.managedProperty().bind(playerTwoScore.visibleProperty());
		customMessage.managedProperty().bind(customMessage.visibleProperty());
		border.getChildren().add(aiType);
		border.getChildren().add(localPlayer);
		border.getChildren().add(playerTurn);
		border.getChildren().add(playerOneScore);
		border.getChildren().add(playerTwoScore);
		border.getChildren().add(moveTimer);
		border.getChildren().add(totalTimer);
		border.getChildren().add(customMessage);
	}

	/**
	 * Update the selected AI
	 *
	 * @param aiType type of AI selected
	 */
	public void setAIType(AIType aiType) {
		Platform.runLater(() -> this.aiType.setText(aiType.toString()));
	}

	/**
	 * Update the time to move timeout
	 *
	 * @param timeoutMilliseconds timeout time in millis
	 */
	public void updateTimeoutMilliseconds(int timeoutMilliseconds) {
		this.timeoutMilliseconds = timeoutMilliseconds;
	}

	/**
	 * Set the pieces of the player (eg: black white X or O)
	 *
	 * @param player player as int
	 */
	public void setLocalPlayer(int player) {
		Platform.runLater(() -> localPlayer.setText("Jij: " + getPlayerString(player)));
	}

	/**
	 * Start the game with a timer
	 */
	public void startGame() {
		gameStartTime = System.currentTimeMillis();
		startTimer();
	}

	/**
	 * Start a timer in a different thread
	 */
	private void startTimer() {
		turnEndTime = System.currentTimeMillis() + timeoutMilliseconds;
		if (timerThread == null || !timerThread.isAlive()) {
			timerThread = new Thread(this::runTimer, "GameStats Timer Thread");
			timerThread.start();
		}
	}

	/**
	 * Update the acting player without the scores
	 *
	 * @param player active player
	 */
	public void startTurn(int player) {
		startTimer();
		Platform.runLater(() -> {
			this.playerTurn.setText("Aan zet: " + getPlayerString(player));
			this.playerOneScore.setVisible(false);
			this.playerTwoScore.setVisible(false);
		});
	}

	/**
	 * Update the acting player and player scores on start of the turn
	 *
	 * @param player         active player
	 * @param playerOneScore score of the first player
	 * @param playerTwoScore score of the second player
	 */
	public void startTurn(int player, int playerOneScore, int playerTwoScore) {
		startTimer();
		Platform.runLater(() -> {
			this.playerTurn.setText("Aan zet: " + getPlayerString(player));
			this.playerOneScore.setVisible(true);
			this.playerTwoScore.setVisible(true);
			this.playerOneScore.setText(playerOne + " score: " + playerOneScore);
			this.playerTwoScore.setText(playerTwo + " score: " + playerTwoScore);
		});
	}

	/**
	 * Stop the timers in the statistics pane
	 */
	public void stopTimer() {
		if (timerThread != null && timerThread.isAlive()) {
			timerThread.interrupt();
		}
	}

	/**
	 * Run the timers in the statistics pane
	 */
	private void runTimer() {
		while (!timerThread.isInterrupted()) {
			long turnDifferenceSeconds = (long) Math
				.max(Math.ceil((turnEndTime - System.currentTimeMillis()) / 1000d), 0);
			long totalTimeSeconds = Math.max((System.currentTimeMillis() - gameStartTime), 0);
			Date date = new Date(totalTimeSeconds);
			Platform.runLater(() -> {
				moveTimer.setText("Timer: " + turnDifferenceSeconds);
				totalTimer.setText("Match tijd: " + dateFormat.format(date));
			});

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ignored) {
				return;
			}
		}
	}

	/**
	 * Show a custom message on the stats panel with the default label color.
	 *
	 * @param message Specifies what message to display
	 */
	public void showCustomMessage(String message) {
		showCustomMessage(message, null);
	}

	/**
	 * Show a custom message on the stats panel with the specified style class.
	 *
	 * @param message Specifies what message to display.
	 * @param styleClass Specifies what style class to add.
	 */
	public void showCustomMessage(String message, String styleClass) {
		Platform.runLater(() -> {
			customMessage.getStyleClass().clear();
			if (styleClass == null) {
				customMessage.getStyleClass().add("game-stats-label");
			} else {
				customMessage.getStyleClass().addAll("game-stats-label", styleClass);
			}
			customMessage.setText(message);
			customMessage.setVisible(true);
		});
	}

	/**
	 * Hides the custom message if it is visible.
	 */
	public void hideCustomMessage() {
		if (!customMessage.isVisible()) {
			return;
		}
		Platform.runLater(() -> {
			customMessage.setVisible(false);
		});
	}

	/**
	 * Convert player as int to player as string
	 *
	 * @param player player as int
	 * @return string of player
	 */
	private String getPlayerString(int player) {
		return player == 1 ? playerOne : playerTwo;
	}
}
