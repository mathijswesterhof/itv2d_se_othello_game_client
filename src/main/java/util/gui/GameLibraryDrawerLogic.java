package util.gui;

import client.FXApplication;
import com.adr.fonticon.IconBuilder;
import com.adr.fonticon.IconFontGlyph;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXRippler;
import com.jfoenix.controls.JFXSlider;
import games.BasePlayer;
import games.ai.AIType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import networking.game.GameType;
import networking.lobby.Lobby;
import networking.player.Player;

public class GameLibraryDrawerLogic {

	private Thread lobbyThread;
	private FXApplication application;
	private Lobby lobby;
	private VBox lobbyBox;
	private JFXDrawer lobbyDrawer;
	private JFXDrawer profileDrawer;
	private AnchorPane fadePane;
	private Label lobbyTitle;
	private Label SelectedAi;
	private VBox profileSelectBox;
	private VBox profileOptionHolder;

	/**
	 * Create logic support for left and right drawer in the lobby
	 *
	 * @param application   root of app
	 * @param lobbyDrawer   left drawer
	 * @param profileDrawer right drawer
	 * @param fadePane      fadepane
	 */
	public GameLibraryDrawerLogic(FXApplication application,
		JFXDrawer lobbyDrawer,
		JFXDrawer profileDrawer,
		AnchorPane fadePane
	) {
		this.application = application;
		this.lobby = new Lobby();
		this.lobbyDrawer = lobbyDrawer;
		this.profileDrawer = profileDrawer;
		this.fadePane = fadePane;
		findSubElementsForLobby();
		addCloseEvents();
		SelectedAi = addAiSelectorToDrawer();
	}

	//	*** Drawers ***

	/**
	 * Add close event to both drawers
	 */
	private void addCloseEvents() {
		lobbyDrawer.setOnDrawerClosed((e) -> {
			if (!(e.getSource() instanceof Node)) {
				return;
			}
			closeLobby();
			toggleFadePane(lobbyDrawer, false);
		});
		profileDrawer.setOnDrawerClosed((e) -> {
			if (!(e.getSource() instanceof Node)) {
				return;
			}
			closeSettings();
			toggleFadePane(profileDrawer, false);
		});
		fadePane.setOnMouseClicked(this::toggleDrawer);
	}

	/**
	 * Toggle drawer open or close
	 *
	 * @param mouseEvent click event
	 */
	private void toggleDrawer(MouseEvent mouseEvent) {
		if (!(mouseEvent.getSource() instanceof Node)) {
			return;
		}

		Node source = (Node) mouseEvent.getSource();
		if (lobbyDrawer.isOpened()) {
			toggleLobby(source);
		}

		if (profileDrawer.isOpened()) {
			toggleProfile(source);
		}
	}

	/**
	 * Convert event to logic for lobby drawer
	 *
	 * @param actionEvent click or drag event
	 */
	public void toggleLobby(ActionEvent actionEvent) {
		if (!(actionEvent.getSource() instanceof Node)) {
			return;
		}
		toggleLobby((Node) actionEvent.getSource());
	}

	/**
	 * Convert event to logic for profile drawer
	 *
	 * @param actionEvent click or drag event
	 */
	public void toggleProfile(ActionEvent actionEvent) {
		if (!(actionEvent.getSource() instanceof Node)) {
			return;
		}
		toggleProfile((Node) actionEvent.getSource());
	}

	/**
	 * Toggle logic for lobby drawer
	 *
	 * @param sourceNode node that fired the event
	 */
	private void toggleLobby(Node sourceNode) {
		if ((lobbyDrawer.isClosed() || lobbyDrawer.isOpening()) && sourceNode != fadePane) {
			GameType gameType = (GameType) sourceNode.getProperties().get("game");
			lobbyBox.getChildren().clear();
			openLobby();
			lobbyBox.getProperties().put("game", gameType);
			lobbyTitle.setText(gameType.getTag() + " Lobby");
			toggleFadePane(lobbyDrawer, true);
			lobbyDrawer.open();
		} else {
			lobbyDrawer.close();
		}
	}

	/**
	 * Toggle logic for profile drawer
	 *
	 * @param sourceNode node that fired the event
	 */
	private void toggleProfile(Node sourceNode) {
		if ((profileDrawer.isClosed() || profileDrawer.isOpening()) && sourceNode != fadePane) {
			toggleFadePane(profileDrawer, true);
			profileDrawer.open();
		} else {
			profileDrawer.close();
		}
	}

	/**
	 * Toggle logic for fade pane
	 *
	 * @param drawer    drawer that has initiated the toggle
	 * @param direction open or close the drawer
	 */
	private void toggleFadePane(JFXDrawer drawer, boolean direction) {
		FadeTransition fade = new FadeTransition(Duration.millis(200), fadePane);
		if (direction) {
			fade.setFromValue(0);
			fade.setToValue(1);
			fade.play();
		} else {
			fade.setFromValue(1);
			fade.setToValue(0);
			fade.play();
		}
		fadePane.setVisible(direction);
		fadePane.setMouseTransparent(!direction);
		drawer.setMouseTransparent(!direction);
	}

	// *** Lobby ***

	/**
	 * Init for lobby drawer
	 */
	private void findSubElementsForLobby() {
		VBox v = (VBox) lobbyDrawer.getSidePane().get(0);
		HBox h = (HBox) v.getChildren().get(0);
		lobbyTitle = (Label) h.getChildren().get(1);
		lobbyBox = (VBox) v.getChildren().get(1);
	}

	/**
	 * Trigger for starting the lobby thread to start looking for opponents
	 */
	private void openLobby() {
		if (lobbyThread != null && lobbyThread.isAlive()) {
			lobbyThread.interrupt();
		}
		lobbyThread = new Thread(this::populateLobby, "Lobby Thread");
		lobbyThread.start();
	}

	/**
	 * Stop lobby thread
	 */
	public void closeLobby() {
		if (lobbyThread == null) {
			return;
		}
		lobbyThread.interrupt();
	}

	/**
	 * Collect players from API and trigger them to be added or removed.
	 */
	private void populateLobby() {
		ArrayList<Player> activePlayers = new ArrayList<>();
		while (!lobbyThread.isInterrupted() && application.isRunning()) {
			try {
				application.getContext().getLobbyStore().updatePlayers(lobby);
				ArrayList<Player> players = lobby.getPlayers();
				ArrayList<Player> disconnected = new ArrayList<>();
				for (Player player : activePlayers) {
					Player tbr = null;
					for (Player current : players) {
						if (player.getUsername().equals(current.getUsername())) {
							tbr = current;
						}
					}
					if (tbr == null) {
						disconnected.add(player);
					}
					players.remove(tbr);
				}
				activePlayers.addAll(players);

				Platform.runLater(() -> {
					removeDisconnectedPlayers(disconnected);
					addNewPlayers(players);
				});

				Thread.sleep(5000);
			} catch (InterruptedException e) {
				lobbyThread.interrupt();
			}
		}
	}

	/**
	 * Remove disconnected players from the drawer
	 *
	 * @param players players to be removed
	 */
	private void removeDisconnectedPlayers(ArrayList<Player> players) {
		ArrayList<String> removed = (ArrayList<String>) players.stream()
			.map(Player::getUsername)
			.collect(Collectors.toList());

		Iterator<Node> it = lobbyBox.getChildren().iterator();
		while (it.hasNext()) {
			Node node = it.next();
			if (!node.getProperties().containsKey("player")) {
				continue;
			}

			Player nodePlayer = (Player) node.getProperties().get("player");
			if (removed.contains(nodePlayer.getUsername())) {
				it.remove();
			}
		}
	}

	/**
	 * Add new players to the drawer
	 *
	 * @param players players to be added
	 */
	private void addNewPlayers(ArrayList<Player> players) {
		for (Player player : players) {
			if (!player.getUsername().equals(
				application.getContext().getCurrentPlayer().getUsername())
			) {
				JFXRippler rippler = makeLobbyEntry("player", player.getUsername());
				rippler.setOnMouseClicked(this::onLobbyEntryClicked);
				rippler.getProperties().put("player", player);
				lobbyBox.getChildren().add(rippler);
			}
		}
	}

	/**
	 * Create a FXML node to add a player to.
	 *
	 * @param type type of player (TODO: in future cases a local opponent can be added)
	 * @param name name of player
	 * @return FXML Node
	 */
	private JFXRippler makeLobbyEntry(String type, String name) {
		IconFontGlyph glyph = (type.equals("player") ?
			IconFontGlyph.FA_SOLID_USER : IconFontGlyph.FA_SOLID_DESKTOP);

		Node icon = IconBuilder.create(glyph, 20).color(Color.WHITE).build();
		HBox.setMargin(icon, new Insets(0, 10, 0, 0));
		HBox iconBox = new HBox();
		iconBox.getChildren().add(icon);

		Label label = new Label(name);
		label.getStyleClass().add("lobbyEntryLabel");

		HBox box = new HBox();
		box.getStyleClass().add("lobbyEntry");
		box.setPadding(new Insets(10));
		box.getChildren().add(iconBox);
		box.getChildren().add(label);

		JFXRippler rippler = new JFXRippler(box);
		rippler.setRipplerFill(Color.WHITE);
		return rippler;
	}

	/**
	 * Handle event that is linked to challenging a player
	 *
	 * @param mouseEvent click on lobbyEntryNode
	 */
	private void onLobbyEntryClicked(MouseEvent mouseEvent) {
		Node sourceNode = (Node) mouseEvent.getSource();
		if (!sourceNode.getProperties().containsKey("player")) {
			return;
		}

		application.getContext().getChallengeStore().challengePlayer(
			application.getContext().getCurrentPlayer(),
			(Player) sourceNode.getProperties().get("player"),
			(GameType) lobbyBox.getProperties().get("game")
		);
	}

	// *** settingsPane

	/**
	 * Init for settings drawer
	 *
	 * @return Label for future toggle
	 */
	private Label addAiSelectorToDrawer() {
		VBox profileDrawerBox = (VBox) profileDrawer.getSidePane().get(0);
		profileOptionHolder = (VBox) profileDrawerBox.getChildren().get(0);
		VBox settings = (VBox) profileOptionHolder.getChildren().get(1);

		profileSelectBox = new VBox();
		profileSelectBox.getChildren().add(makeProfileSelectHeader());
		for (AIType t : AIType.values()) {
			profileSelectBox.getChildren().add(
				makeAISelectEntry(t.toString(), (e) -> selectAI(t))
			);
		}

		return makePlayTypeSelector(settings);
	}

	/**
	 * Update settings from the settings tab after closing the settings tab
	 */
	private void closeSettings() {
		VBox settings = (VBox) profileDrawer.getSidePane().get(0);
		settings = (VBox) settings.getChildren().get(0);
		if (settings.getChildren().get(1) instanceof VBox) {
			settings = (VBox) settings.getChildren().get(1);
			for (Node n : settings.getChildren()) {
				if (n instanceof JFXSlider) {
					JFXSlider slider = (JFXSlider) n;
					application.getContext().setTurnTimeout(slider.getValue());
				}
			}
		}
	}

	/**
	 * Create FXML node that acts as a multi select box
	 *
	 * @return Node
	 */
	private Node makeProfileSelectHeader() {
		Node icon = IconBuilder.create(IconFontGlyph.FA_SOLID_ARROW_LEFT, 16).color(Color.WHITE)
			.build();
		HBox iconBox = new HBox();
		iconBox.setPadding(new Insets(8));
		iconBox.getChildren().add(icon);
		JFXRippler rippler = new JFXRippler(iconBox);
		rippler.setOnMouseClicked((e) -> {
			VBox activePane = (VBox) profileDrawer.getSidePane().get(0);
			activePane.getChildren().remove(profileSelectBox);
			activePane.getChildren().add(profileOptionHolder);
		});
		rippler.setRipplerFill(Color.WHITE);

		Label label = new Label("Selecteer A.I.");
		label.getStyleClass().add("profileHeaderLabel");

		HBox box = new HBox();
		HBox.setMargin(rippler, new Insets(0, 10, 0, 0));
		box.getStyleClass().add("drawerTitleBox");
		box.setPadding(new Insets(10));
		box.getChildren().add(rippler);
		box.getChildren().add(label);
		return box;
	}

	/**
	 * Create FXML node that acts as a select box entry
	 *
	 * @param labelText    option that can be selected
	 * @param eventHandler event to be triggered after select
	 * @return Node
	 */
	private Node makeAISelectEntry(String labelText,
		EventHandler<? super MouseEvent> eventHandler) {
		Label label = new Label(labelText);
		label.getStyleClass().add("aiSelectLabel");

		HBox box = new HBox();
		box.getStyleClass().add("selectEntry");
		box.setPadding(new Insets(10));
		box.getChildren().add(label);

		JFXRippler rippler = new JFXRippler(box);
		rippler.setOnMouseClicked(eventHandler);
		rippler.setRipplerFill(Color.WHITE);
		return rippler;
	}

	/**
	 * Resolved triggered event from selected entry
	 *
	 * @param aiType currently selected AI
	 */
	private void selectAI(AIType aiType) {
		application.getContext().getCurrentPlayer().setAI(aiType);
		SelectedAi.setText(String.format("Speeltype: %s", aiType.toString()));
		VBox activePane = (VBox) profileDrawer.getSidePane().get(0);
		activePane.getChildren().remove(profileSelectBox);
		activePane.getChildren().add(profileOptionHolder);
	}

	/**
	 * Create a selector that displays current play type
	 *
	 * @return Label to return for future refs
	 */
	private Label makePlayTypeSelector(VBox settings) {
		Node icon = IconBuilder.create(IconFontGlyph.FA_SOLID_CHEVRON_RIGHT, 14).color(Color.WHITE)
			.build();

		BasePlayer player = application.getContext().getCurrentPlayer();
		String labelText = String.format("Speeltype: %s",
			(player == null ? AIType.MANUAL : player.getAIType().toString())
		);

		Label label = new Label(labelText);
		label.getStyleClass().add("lobbyEntryLabel");

		AnchorPane.setLeftAnchor(label, 10d);
		AnchorPane.setTopAnchor(label, 10d);
		AnchorPane.setBottomAnchor(label, 10d);

		AnchorPane.setRightAnchor(icon, 10d);
		AnchorPane.setTopAnchor(icon, 16d);
		AnchorPane.setBottomAnchor(icon, 16d);

		AnchorPane pane = new AnchorPane();
		pane.getStyleClass().addAll("lobbyEntry", "bottomBorder");
		pane.getChildren().add(label);
		pane.getChildren().add(icon);

		JFXRippler rippler = new JFXRippler(pane);
		rippler.setRipplerFill(Color.WHITE);
		rippler.setOnMouseClicked((e) -> {
			VBox activePane = (VBox) profileDrawer.getSidePane().get(0);
			activePane.getChildren().remove(profileOptionHolder);
			activePane.getChildren().add(profileSelectBox);
		});
		settings.getChildren().add(0, rippler);
		return label;
	}
}
