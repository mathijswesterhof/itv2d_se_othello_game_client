package util.gui;

import com.jfoenix.controls.JFXDialogLayout;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public class DialogBuilder {

	private static boolean tournamentMode = false;
	private HashMap<String, Dialog> dialogs;

	/**
	 * Create list for build dialogs
	 */
	public DialogBuilder() {
		dialogs = new HashMap<>();
	}

	/**
	 * Create dialog with body
	 *
	 * @param root    root for dialog
	 * @param heading title for dialog
	 * @param body    body of dialog
	 * @param name    namespace
	 * @return Dialog
	 */
	public Dialog createNewDialog(StackPane root, String heading, Node body, String name) {
		JFXDialogLayout layout = new JFXDialogLayout();
		layout.setBody(body);
		Dialog dialog = new Dialog(root, layout, heading);
		dialogs.put(name, dialog);
		return dialog;
	}

	/**
	 * Create a new dialog based on specs
	 *
	 * @param root    root for dialog
	 * @param heading title of dialog
	 * @param name    namespace
	 * @return Dialog
	 */
	public Dialog createNewDialog(StackPane root, String heading, String name) {
		JFXDialogLayout layout = new JFXDialogLayout();
		Dialog dialog = new Dialog(root, layout, heading);
		dialogs.put(name, dialog);
		return dialog;
	}

	/**
	 * Check if tournament mode is active
	 *
	 * @return boolean
	 */
	public boolean isTournamentMode() {
		return tournamentMode;
	}

	/**
	 * Set tournament mode (some dialogs will not be shown)
	 *
	 * @param mode boolean
	 */
	public void setTournamentMode(boolean mode) {
		tournamentMode = mode;
	}

	/**
	 * Close all dialogs in list
	 */
	public void clearDialogs() {
		for (Dialog dialog : dialogs.values()) {
			dialog.close();
		}
		dialogs.clear();
	}
}
