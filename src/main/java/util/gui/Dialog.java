package util.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class Dialog extends JFXDialog {

	private JFXDialogLayout layout;

	/**
	 * Create extended JFXDialog
	 *
	 * @param root    stackpane the dialog lives in
	 * @param layout  default layout
	 * @param heading title
	 */
	Dialog(StackPane root, JFXDialogLayout layout, String heading) {
		super(root, layout, DialogTransition.CENTER);
		super.getContent().getStyleClass().add("dialogContent");
		super.getStyleClass().add("dialog");
		super.setOverlayClose(false);
		Label title = new Label(heading);
		title.setTextFill(Color.WHITE);
		layout.setHeading(title);
		this.layout = layout;
	}

	/**
	 * Add button to Dialog
	 *
	 * @param name label for button
	 * @return button
	 */
	public JFXButton addButton(String name) {
		JFXButton addedButton = new JFXButton(name);
		addedButton.getStyleClass().add("borderButton");
		layout.setActions(addedButton);
		return addedButton;
	}

	/**
	 * Add multiple buttons to dialog
	 *
	 * @param names labels for the different buttons
	 * @return array of buttons
	 */
	public JFXButton[] addButtons(String[] names) {
		JFXButton[] buttons = new JFXButton[names.length];
		HBox actionBox = new HBox();
		actionBox.setAlignment(Pos.CENTER_RIGHT);
		for (int n = 0; n < names.length; n++) {
			buttons[n] = new JFXButton(names[n]);
			buttons[n].getStyleClass().add("borderButton");
			actionBox.getChildren().add(buttons[n]);
		}
		layout.setActions(actionBox);
		return buttons;
	}
}
