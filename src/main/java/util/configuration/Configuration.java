package util.configuration;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Properties;

public class Configuration {

	private String description;
	private String filename;
	private File configurationFile;

	private Properties properties = new Properties();

	/**
	 * Create a configuration file
	 *
	 * @param filename    name of file
	 * @param description description
	 */
	public Configuration(String filename, String description) {
		setFilename(filename);
		setDescription(description);
		loadFile();
		loadProperties();
	}

	/**
	 * Load file from resource
	 */
	private void loadFile() {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			setConfigurationFile(new File(classLoader.getResource(filename).toURI()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Get properties from file
	 */
	private void loadProperties() {
		try {
			FileReader fileReader = new FileReader(getConfigurationFile());
			getProperties().load(fileReader);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Save properties to file
	 */
	public void save() {
		try {
			FileWriter fileWriter = new FileWriter(getConfigurationFile());
			getProperties().store(fileWriter, getDescription());
			fileWriter.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Get a property by key
	 *
	 * @param key key of property
	 * @return String belonging to key
	 */
	public String getProperty(String key) {
		return getProperties().getProperty(key);
	}

	/**
	 * Get description of file
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set description of file
	 *
	 * @param description description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get file
	 *
	 * @return File
	 */
	public File getConfigurationFile() {
		return configurationFile;
	}

	/**
	 * set file
	 *
	 * @param configurationFile file
	 */
	private void setConfigurationFile(File configurationFile) {
		this.configurationFile = configurationFile;
	}

	/**
	 * Set name of file
	 *
	 * @param filename name
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Get properties from configuration
	 *
	 * @return Properties
	 */
	public Properties getProperties() {
		return properties;
	}

}
