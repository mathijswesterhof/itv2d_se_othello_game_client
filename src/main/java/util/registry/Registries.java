package util.registry;

import client.FXApplication;
import client.gui.controller.GameLibraryController;
import client.gui.controller.LoginController;
import client.gui.controller.WelcomeController;
import games.Reversi.ReversiController;
import games.TicTacToe.TicTacToeController;
import games.ai.AI;
import games.ai.AIType;
import games.ai.MinimaxEasyAI;
import games.ai.MinimaxHardAI;
import games.ai.MinimaxMediumAI;
import games.ai.RandomValidMoveAI;
import javafx.scene.Scene;
import networking.game.GameType;

public class Registries {

	public static final Registry<Scene> SCENE = new Registry<>("scene");
	public static final Registry<AI> AI = new Registry<>("ai");

	/**
	 * Register the different scenes to the main application
	 *
	 * @param application main view
	 */
	public static void registerScenes(FXApplication application) {
		Registries.SCENE.register("login", LoginController.loadScene(application));
		Registries.SCENE.register("welcome", WelcomeController.loadScene(application));
		Registries.SCENE.register("game_library", GameLibraryController.loadScene(application));
		Registries.SCENE.register(
			GameType.TIC_TAC_TOE.getTag(),
			TicTacToeController.loadScene(application)
		);
		Registries.SCENE.register(
			GameType.REVERSI.getTag(),
			ReversiController.loadScene(application)
		);
	}

	/**
	 * Register the different types of AI
	 */
	public static void registerAIs() {
		Registries.AI.register(AIType.MANUAL.toString(), null);
		Registries.AI.register(AIType.RANDOMVALID.toString(), new RandomValidMoveAI());
		Registries.AI.register(AIType.MINIMAX_EASY.toString(), new MinimaxEasyAI());
		Registries.AI.register(AIType.MINIMAX_MEDIUM.toString(), new MinimaxMediumAI());
		Registries.AI.register(AIType.MINIMAX_HARD.toString(), new MinimaxHardAI());
	}
}
