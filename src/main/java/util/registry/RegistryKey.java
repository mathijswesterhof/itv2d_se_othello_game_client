package util.registry;

public class RegistryKey {

	private final String namespace;
	private final String registryName;
	private final String targetName;
	private int hash;

	/**
	 * Create new RegistryKey
	 *
	 * @param namespace    namespace
	 * @param registryName name of registry
	 * @param targetName   name of target
	 */
	public RegistryKey(String namespace, String registryName, String targetName) {
		this.namespace = namespace;
		this.registryName = registryName;
		this.targetName = targetName;
	}

	/**
	 * Get namespace
	 *
	 * @return namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * Get registry name
	 *
	 * @return name
	 */
	public String getRegistryName() {
		return registryName;
	}

	/**
	 * Get name
	 *
	 * @return name
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * Compare two registry objects
	 *
	 * @param o object
	 * @return boolean
	 */
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o instanceof RegistryKey) {
			RegistryKey registryKey = (RegistryKey) o;
			return registryKey.getNamespace().equals(namespace) && registryKey.getRegistryName()
				.equals(registryName) && registryKey.getTargetName().equals(targetName);
		}

		return false;
	}

	/**
	 * Return a hash code for the associated string. Required for usage as key in the registry hash
	 * map.
	 *
	 * @return a hash code value for this object.
	 */
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * Return a string representation of this key.
	 *
	 * @return associated string value for this object.
	 */
	public String toString() {
		return String.format("%s.%s.%s", namespace, registryName, targetName);
	}
}
