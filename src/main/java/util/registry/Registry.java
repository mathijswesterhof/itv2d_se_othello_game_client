package util.registry;

import java.util.HashMap;

public class Registry<T> {

	private static final String DEFAULT_NAMESPACE = "counter";
	protected final HashMap<RegistryKey, T> entries = new HashMap<>();
	private final String registryName;

	/**
	 * Create a new Registry
	 *
	 * @param registryName identifier
	 */
	public Registry(String registryName) {
		this.registryName = registryName;
	}

	/**
	 * Get item from the default namespace
	 *
	 * @param targetName itemName
	 * @return item
	 */
	public T get(String targetName) {
		return get(DEFAULT_NAMESPACE, targetName);
	}

	/**
	 * Get item from custom namespace
	 *
	 * @param namespace  namespace
	 * @param targetName itemName
	 * @return item
	 */
	public T get(String namespace, String targetName) {
		return get(formatKey(namespace, targetName));
	}

	/**
	 * Get entries from registry key
	 *
	 * @param registryKey registry key
	 * @return entries
	 */
	public T get(RegistryKey registryKey) {
		return entries.get(registryKey);
	}

	/**
	 * Register item with name in default namespace
	 *
	 * @param targetName name
	 * @param target     item
	 */
	public void register(String targetName, T target) {
		register(DEFAULT_NAMESPACE, targetName, target);
	}

	/**
	 * Register item with name in custom namespace
	 *
	 * @param namespace  namespace
	 * @param targetName name
	 * @param target     item
	 */
	public void register(String namespace, String targetName, T target) {
		register(formatKey(namespace, targetName), target);
	}

	/**
	 * Put entry into register by key
	 *
	 * @param registryKey register key
	 * @param target      entry
	 */
	public void register(RegistryKey registryKey, T target) {
		entries.put(registryKey, target);
	}

	/**
	 * Check if registry contains key
	 *
	 * @param registryKey key
	 * @return boolean
	 */
	public boolean containsKey(RegistryKey registryKey) {
		return entries.containsKey(registryKey);
	}

	/**
	 * Create new key for namespace with name
	 *
	 * @param namespace  namespace
	 * @param targetName name
	 * @return registryKey
	 */
	private RegistryKey formatKey(String namespace, String targetName) {
		return new RegistryKey(namespace, registryName, targetName);
	}
}
