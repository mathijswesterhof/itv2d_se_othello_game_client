package client;

public interface ApplicationListener {

	/**
	 * Closing procedures for the applet
	 */
	void onSaveAndExit();
}
