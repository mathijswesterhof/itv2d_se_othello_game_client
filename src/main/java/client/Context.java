package client;

import games.BasePlayer;
import networking.NetworkAPI;
import networking.challenge.ChallengeStore;
import networking.game.GameStore;
import networking.lobby.LobbyStore;
import networking.player.PlayerStore;
import networking.protocol.MessageListener;
import util.configuration.Configuration;

public class Context {

	private NetworkAPI apiConnection;
	private int turnTimeoutMilliseconds = 10000;
	private Configuration configuration;

	/**
	 * Create a context with a configuration file
	 */
	public Context() {
		configuration = new Configuration("configuration.properties", "Configuration");
	}

	/**
	 * Create a connection to the server trough the API
	 *
	 * @param ip   IP of the server
	 * @param port port to connect to
	 */
	public void setupConnection(String ip, int port) {
		if (!hasNetworkAPI()) {
			this.apiConnection = new NetworkAPI();
			this.apiConnection.openConnection(ip, port);
		}
	}

	/**
	 * Get the current logged in player
	 *
	 * @return Instance of a logged in player or NULL
	 */
	public BasePlayer getCurrentPlayer() {
		if (hasNetworkAPI()) {
			return (BasePlayer) apiConnection.getPlayerStore().getCurrentPlayer();
		} else {
			return null;
		}
	}

	/**
	 * Set the current player
	 *
	 * @param player player instance
	 */
	public void setCurrentPlayer(BasePlayer player) {
		apiConnection.getPlayerStore().setCurrentPlayer(player);
	}

	/**
	 * Get the api PlayerStore
	 *
	 * @return PlayerStore
	 */
	public PlayerStore getPlayerStore() {
		return apiConnection.getPlayerStore();
	}

	/**
	 * Get the api GameStore
	 *
	 * @return GameStore
	 */
	public GameStore getGameStore() {
		return apiConnection.getGameStore();
	}

	/**
	 * Get the api LobbyStore
	 *
	 * @return LobbyStore
	 */
	public LobbyStore getLobbyStore() {
		return apiConnection.getLobbyStore();
	}

	/**
	 * Get the api ChallengeStore
	 *
	 * @return ChallengeStore
	 */
	public ChallengeStore getChallengeStore() {
		return apiConnection.getChallengeStore();
	}

	/**
	 * set a message listener in the API
	 *
	 * @param messageListener message listener to set
	 */
	public void addMessageListener(MessageListener messageListener) {
		if (!apiConnection.hasMessageListener(messageListener)) {
			apiConnection.addMessageListener(messageListener);
		}
	}

	/**
	 * Update the timeout for taking turns
	 *
	 * @param seconds timeout
	 */
	public void setTurnTimeout(double seconds) {
		turnTimeoutMilliseconds = (int) seconds * 1000;
	}

	/**
	 * Get the current timeout for taking turns
	 *
	 * @return time in millis
	 */
	public int getTurnTimeoutMilliseconds() {
		return turnTimeoutMilliseconds;
	}

	/**
	 * Close the application
	 */
	public void close() {
		if (apiConnection == null) {
			return;
		}

		apiConnection.close();
	}

	/**
	 * Get the configuration
	 *
	 * @return Configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * Check if there is a connection
	 *
	 * @return boolean if true
	 */
	public boolean hasNetworkAPI() {
		return apiConnection != null;
	}

	/**
	 * Reset the new network connection
	 */
	public void resetNetworkAPI() {
		if (hasNetworkAPI()) {
			apiConnection.getConnection().closeConnection();
			this.apiConnection = null;
		}
	}
}
