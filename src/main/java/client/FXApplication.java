package client;

import client.gui.scene.GameCounterScene;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.gui.Loader;
import util.registry.Registries;

public class FXApplication extends Application {

	private static final String TITLE = "Game Counter";
	private final ArrayList<ApplicationListener> applicationListeners;
	private final Thread gameThread;
	private final Context context;
	private volatile boolean running = true;
	private volatile Stage stage;

	/**
	 * Create the root of the application
	 */
	public FXApplication() {
		applicationListeners = new ArrayList<>();
		context = new Context();
		gameThread = new Thread(this::run, "Game Thread");
	}

	/**
	 * Launch the root pane (first screen)
	 *
	 * @param args args
	 */
	public static void launch(String[] args) {
		Application.launch(args);
	}

	/**
	 * Create the outer window
	 *
	 * @param stage outer stage
	 */
	public void start(Stage stage) {
		this.stage = stage;
		stage.getIcons().add(Loader.loadImage("image/icon_gc.png"));

		//Set window title
		stage.setTitle(TITLE);

		//Register components
		Registries.registerScenes(this);
		Registries.registerAIs();

		//Start game
		gameThread.start();
		stage.toFront();
	}

	/**
	 * Start the Gui thread and open the login window
	 */
	public void run() {
		try {
			int state = 0;
			while (running && !Thread.currentThread().isInterrupted()) {
				Thread.onSpinWait();
				if (state == 0) {
					showScene(Registries.SCENE.get("login"));
					state = 1;
				}
			}
		} catch (Throwable throwable) {
			System.err.println(String.format("Unhandled game exception: %s", throwable));
			throwable.printStackTrace();
		}

		saveAndExit();
	}

	/**
	 * Show a new Scene in the window
	 *
	 * @param scene scene to be shown
	 */
	public void showScene(Scene scene) {
		Platform.runLater(() -> {
			if (scene instanceof GameCounterScene) {
				GameCounterScene gameCounterScene = (GameCounterScene) scene;
				if (gameCounterScene.hasController()) {
					gameCounterScene.getController().onShow();
				}
			}

			stage.setScene(scene);
			stage.setHeight(800);
			stage.setWidth(1000);
			stage.show();
		});
	}

	/**
	 * Close the main application and stop the Gui thread
	 */
	private void saveAndExit() {
		raiseOnSaveAndExit();
		gameThread.interrupt();
		if (context.hasNetworkAPI()) {
			context.getPlayerStore().logoutPlayer(context.getCurrentPlayer());
		}

		Platform.exit();
		running = false;
	}

	@Override
	public void stop() {
		saveAndExit();
	}

	/**
	 * Get context
	 *
	 * @return context
	 */
	public Context getContext() {
		return context;
	}

	/**
	 * Check if the application is still running
	 *
	 * @return boolean if true
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Add an application listener
	 *
	 * @param applicationListener added listener
	 */
	public void addApplicationListener(ApplicationListener applicationListener) {
		applicationListeners.add(applicationListener);
	}

	/**
	 * Event for Quitting the Window
	 */
	public void raiseOnSaveAndExit() {
		for (ApplicationListener applicationListener : applicationListeners) {
			applicationListener.onSaveAndExit();
		}
	}
}
