package client.gui.controller;

import client.ApplicationListener;
import client.FXApplication;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXToggleButton;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import networking.challenge.Challenge;
import networking.game.Game;
import networking.game.GameType;
import networking.player.Player;
import networking.protocol.MessageListener;
import util.gui.Dialog;
import util.gui.DialogBuilder;
import util.gui.GameLibraryDrawerLogic;
import util.gui.GameLibraryGameLoader;
import util.gui.Loader;
import util.registry.Registries;

public class GameLibraryController extends GameCounterController implements MessageListener,
	ApplicationListener {

	@FXML
	private Label username;
	@FXML
	private Label profileUsername;
	@FXML
	private JFXButton profileButton;
	@FXML
	private JFXDrawer lobbyDrawer;
	@FXML
	private JFXDrawer profileDrawer;
	@FXML
	private StackPane dialogPane;
	@FXML
	private AnchorPane fadePane;
	@FXML
	private AnchorPane root;
	@FXML
	private VBox lobbyDrawerBox;
	@FXML
	private VBox profileDrawerBox;
	@FXML
	private HBox GameSelector;

	private Challenge currentChallenge;
	private DialogBuilder dialogBuilder;

	private GameLibraryDrawerLogic gameLibraryLogic;

	/**
	 * Get the FXML view and load it into the application
	 *
	 * @param application root
	 * @return the current scene
	 */
	public static Scene loadScene(FXApplication application) {
		return Loader.loadFXML(application, "scene/game_library.fxml");
	}

	@Override
	protected void onLoad() {
		application.addApplicationListener(this);

		lobbyDrawer.setSidePane(lobbyDrawerBox);
		lobbyDrawer.setMouseTransparent(true);
		profileDrawer.setSidePane(profileDrawerBox);
		profileDrawer.setMouseTransparent(true);

		gameLibraryLogic = new GameLibraryDrawerLogic(application, lobbyDrawer, profileDrawer,
			fadePane);
		GameLibraryGameLoader gameLibraryGameLoader = new GameLibraryGameLoader(GameSelector);
		dialogBuilder = new DialogBuilder();

		for (GameType g : GameType.values()) {
			gameLibraryGameLoader.addNode(g,
				"Lorem ipsum dolor sit amte, consectetur adipiscing elit, sed do eiusmod tempor");
		}

		profileButton.setOnAction(gameLibraryLogic::toggleProfile);
		gameLibraryGameLoader.setLobby(gameLibraryLogic::toggleLobby);
		gameLibraryGameLoader.setQuickPlay(this::quickPlay);
	}

	// *** settingEvents ***

	/**
	 * Toggle tournament mode (some dialogs get omitted)
	 *
	 * @param event button that was clicked
	 */
	public void toggleTournamentMode(MouseEvent event) {
		JFXToggleButton button = (JFXToggleButton) event.getSource();
		dialogBuilder.setTournamentMode(button.isSelected());
	}

	// *** Popups ***

	/**
	 * Trigger a popup that shows the game is waiting for an opponent
	 *
	 * @param actionEvent button that was clicked
	 */
	private void quickPlay(ActionEvent actionEvent) {
		JFXSpinner spinner = new JFXSpinner();
		spinner.getStyleClass().add("jfxSpinner-blueGradient");
		VBox.setMargin(spinner, new Insets(20, 0, 0, 0));

		Dialog dialog = dialogBuilder.createNewDialog(
			dialogPane,
			"Wachten op tegenstander...",
			spinner,
			"Quickplay"
		);
		JFXButton cancelButton = dialog.addButton("Cancel");
		cancelButton.setOnAction(event -> dialog.close());

		dialog.setOnDialogClosed((e) -> {
			dialogPane.setMouseTransparent(true);
			application.getContext().getLobbyStore().unsubscribeFromGame(
				(GameType) ((Node) actionEvent.getSource()).getProperties().get("game")
			);
		});
		dialog.setOnDialogOpened((e) -> {
			root.setMouseTransparent(false);
			dialogPane.setMouseTransparent(false);
			application.getContext().getLobbyStore().subscribeToGame(
				(GameType) ((Node) actionEvent.getSource()).getProperties().get("game"));

		});
		dialog.show();
	}

	/**
	 * Show to the player an opponent wants to challenge the player
	 *
	 * @param challenge challenge to be accepted or denied
	 */
	private void showChallenge(Challenge challenge) {
		if (dialogBuilder.isTournamentMode()) {
			application.getContext().getChallengeStore().acceptChallenge(currentChallenge);
			return;
		}
		Dialog dialog = dialogBuilder.createNewDialog(
			dialogPane,
			String.format("Uitdaging ontvangen van %s", challenge.getChallenger().getUsername()),
			"ShowChallenge"
		);
		JFXButton[] buttons = dialog.addButtons(new String[]{"Accepteren", "Weigeren"});

		buttons[0].setOnAction((e) -> {
			if (currentChallenge != null) {
				application.getContext().getChallengeStore().acceptChallenge(currentChallenge);
			}
			dialog.close();

		});
		buttons[1].setOnAction((e) -> {
			if (currentChallenge != null) {
				application.getContext().getChallengeStore().cancelChallenge(currentChallenge);
			}
			dialog.close();
		});
		dialog.setOnDialogClosed((e) -> dialogPane.setMouseTransparent(true));
		dialog.setOnDialogOpened((e) -> dialogPane.setMouseTransparent(false));
		root.setMouseTransparent(false);
		dialog.show();
	}

	// *** Overrides ***
	@Override
	public void onShow() {
		application.getContext().addMessageListener(this);
		Player player = application.getContext().getCurrentPlayer();
		if (player == null) {
			username.setText("no_login");
			return;
		}

		username.setText(application.getContext().getCurrentPlayer().getUsername());
		profileUsername.setText(application.getContext().getCurrentPlayer().getUsername());
	}

	@Override
	public void onMatchFound(Game game) {
		Platform.runLater(() -> {
			dialogBuilder.clearDialogs();
			gameLibraryLogic.closeLobby();
			application.showScene(Registries.SCENE.get(game.getGameType().getTag()));
		});
	}

	@Override
	public void onChallengeReceived(Challenge challenge) {
		Platform.runLater(() -> {
			if (currentChallenge != null) {
				dialogBuilder.clearDialogs();
				application.getContext().getChallengeStore().cancelChallenge(challenge);
			}

			currentChallenge = challenge;
			showChallenge(challenge);
		});
	}

	@Override
	public void onChallengeCanceled(int id) {
		Platform.runLater(() -> dialogBuilder.clearDialogs());
		currentChallenge = null;
	}

	@Override
	public void onSaveAndExit() {
		gameLibraryLogic.closeLobby();
	}
}

