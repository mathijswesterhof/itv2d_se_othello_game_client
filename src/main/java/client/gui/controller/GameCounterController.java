package client.gui.controller;

import client.FXApplication;

public abstract class GameCounterController {

	protected FXApplication application;

	/**
	 * triggers when loading the controller for the first time
	 */
	protected void onLoad() {
	}

	/**
	 * Triggers when the controller view gets shown
	 */
	public void onShow() {
	}

	/**
	 * Get the FXML root application
	 *
	 * @return FXML root
	 */
	public FXApplication getApplication() {
		return application;
	}

	/**
	 * Set the FXML root application
	 *
	 * @param application root
	 */
	public void setApplication(FXApplication application) {
		this.application = application;
		onLoad();
	}
}
