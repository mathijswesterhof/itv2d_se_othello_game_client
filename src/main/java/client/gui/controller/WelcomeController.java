package client.gui.controller;

import client.FXApplication;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import util.gui.Loader;
import util.registry.Registries;

public class WelcomeController extends GameCounterController {

	@FXML
	private Label username;
	private ScheduledExecutorService executor;

	/**
	 * Get the FXML view and load it into the application
	 *
	 * @param application root
	 * @return the current scene
	 */
	public static Scene loadScene(FXApplication application) {
		return Loader.loadFXML(application, "scene/welcome.fxml");
	}

	@Override
	public void onShow() {
		username.setText(application.getContext().getCurrentPlayer().getUsername());
		executor = Executors.newSingleThreadScheduledExecutor();
		executor.schedule(this::nextScene, 2, TimeUnit.SECONDS);
	}

	/**
	 * Show the lobby after the welcome screen timeout
	 */
	private void nextScene() {
		application.showScene(Registries.SCENE.get("game_library"));
		executor.shutdown();
		executor = null;
	}
}
