package client.gui.controller;

import client.FXApplication;
import client.gui.validation.TextFieldLengthValidator;
import client.gui.validation.UsernameAvailabilityValidator;
import client.gui.validation.UsernameContentValidator;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import games.BasePlayer;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import util.gui.Loader;
import util.registry.Registries;

public class LoginController extends GameCounterController {

	@FXML
	private JFXTextField server;
	@FXML
	private JFXTextField port;
	@FXML
	private JFXTextField username;

	/**
	 * Get the FXML view and load it into the application
	 *
	 * @param application root
	 * @return the current scene
	 */
	public static Scene loadScene(FXApplication application) {
		return Loader.loadFXML(application, "scene/login.fxml");
	}

	@Override
	protected void onLoad() {
		//TODO: localization.
		server.setText(application.getContext().getConfiguration().getProperty("server.ip"));
		port.setText(application.getContext().getConfiguration().getProperty("server.port"));

		addIntegerCheck();
	}

	@Override
	public void onShow() {
		username.requestFocus();
	}

	@FXML
	public void login() {
		if (application.getContext().hasNetworkAPI()) {
			application.getContext().resetNetworkAPI();
		}

		application.getContext()
			.setupConnection(server.getText(), Integer.parseInt(port.getText()));

		if (username == null) {
			return;
		}

		username.getValidators().clear();
		username.getValidators().add(
			new RequiredFieldValidator("Gebruikersnaam mag niet leeg zijn."));
		username.getValidators().add(
			new TextFieldLengthValidator(1, 64,
				"Gebruikersnaam"));
		username.getValidators().add(
			new UsernameContentValidator(application.getContext().getPlayerStore(),
				"Alleen ASCII tekens zijn toegestaan"));
		username.getValidators().add(
			new UsernameAvailabilityValidator(application.getContext().getPlayerStore(),
				"Gebruikersnaam is al in gebruik."));

		if (!username.validate()) {
			username.requestFocus();
			return;
		}

		BasePlayer localPlayer = new BasePlayer(username.getText(), false);
		if (application.getContext().getPlayerStore().authorizePlayer(localPlayer)) {
			application.getContext().setCurrentPlayer(localPlayer);
			application.showScene(Registries.SCENE.get("welcome"));
		} else {
			throw new RuntimeException("Could not authorize player");
		}
	}

	/**
	 * Check for a valid integer number and place it in the port property if it is
	 */
	private void addIntegerCheck() {
		port.textProperty().addListener((observable, old, newInput) -> {
			if (!newInput.matches("\\d*")) {
				port.setText(newInput.replaceAll("[^\\d]", ""));
			}
		});
	}
}
