package client.gui.validation;

import javafx.beans.DefaultProperty;

/**
 * The <code>TextFieldLengthValidator</code> class is used to validate jfoenix text fields.
 */
@DefaultProperty(value = "icon")
public class TextFieldLengthValidator extends TextFieldValidator {

	private int minLength;
	private int maxLength;

	/**
	 * Construct a TextFieldLengthValidator with the specified arguments.
	 *
	 * @param minLength Specifies the minimum length required to pass validation.
	 * @param maxLength Specifies the maximul length allowed to pass validation.
	 * @param fieldName Specifies the associated field name.
	 */
	public TextFieldLengthValidator(int minLength, int maxLength, String fieldName) {
		super(String.format("%s moet %d-%d tekens lang zijn.", fieldName, minLength, maxLength));
		this.minLength = minLength;
		this.maxLength = maxLength;
	}

	@Override
	protected boolean evalContent(String string) {
		return string != null && string.length() >= minLength && string.length() < maxLength;
	}

}
