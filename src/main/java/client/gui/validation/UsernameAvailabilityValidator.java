package client.gui.validation;

import javafx.beans.DefaultProperty;
import networking.player.PlayerStore;

/**
 * The <code>UsernameAvailabilityValidator</code> class is used to validate username availability.
 */
@DefaultProperty(value = "icon")
public class UsernameAvailabilityValidator extends UsernameValidator {

	/**
	 * Construct a UsernameAvailabilityValidator with the specified parameters.
	 *
	 * @param playerStore Specifies the player store to use to validate the username.
	 * @param message     Specifies what message to display if validation fails.
	 */
	public UsernameAvailabilityValidator(PlayerStore playerStore, String message) {
		super(playerStore, message);
	}

	@Override
	protected boolean evalContent(String string) {
		return this.playerStore.checkUsernameAvailability(string);
	}
}
