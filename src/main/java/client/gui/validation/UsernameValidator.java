package client.gui.validation;

import javafx.beans.DefaultProperty;
import networking.player.PlayerStore;

/**
 * The <code>UsernameValidator</code> class is used to validate username fields.
 */
@DefaultProperty(value = "icon")
public abstract class UsernameValidator extends TextFieldValidator {

	protected final PlayerStore playerStore;

	/**
	 * Construct a UsernameValidator with the specified parameters.
	 *
	 * @param playerStore Specifies the player store to use to validate the username.
	 * @param message     Specifies what message to display if validation fails.
	 */
	public UsernameValidator(PlayerStore playerStore, String message) {
		super(message);
		this.playerStore = playerStore;
	}
}
