package client.gui.validation;

import javafx.beans.DefaultProperty;
import networking.player.PlayerStore;

/**
 * The <code>UsernameContentValidator</code> class is used to validate username content.
 */
@DefaultProperty(value = "icon")
public class UsernameContentValidator extends UsernameValidator {

	/**
	 * Construct a UsernameContentValidator with the specified parameters.
	 *
	 * @param playerStore Specifies the player store to use to validate the username.
	 * @param message     Specifies what message to display if validation fails.
	 */
	public UsernameContentValidator(PlayerStore playerStore, String message) {
		super(playerStore, message);
	}

	@Override
	protected boolean evalContent(String username) {
		return this.playerStore.validateUsername(username);
	}
}
