package client.gui.validation;

import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.TextInputControl;

/**
 * The <code>TextFieldValidator</code> represents a validator used in jfoenix text fields.
 */
public abstract class TextFieldValidator extends ValidatorBase {

	/**
	 * Construct a TextFieldValidator with the specified parameters.
	 *
	 * @param message Specifies the message to display if validation fails.
	 */
	public TextFieldValidator(String message) {
		super(message);
	}

	/**
	 * Evaluates weather or not the text fields content is valid or not.
	 *
	 * @param string Specifies what text to validate.
	 * @return The validation result.
	 */
	protected abstract boolean evalContent(String string);

	@Override
	protected void eval() {
		boolean valid = false;
		if (srcControl.get() instanceof TextInputControl) {
			valid = evalTextInputField();
		}

		if (srcControl.get() instanceof ComboBoxBase) {
			valid = evalComboBoxField();
		}

		hasErrors.set(!valid);
	}

	/**
	 * Handles validation for a <code>TextInputControl</code>.
	 *
	 * @return The validation result.
	 */
	private boolean evalTextInputField() {
		TextInputControl textField = (TextInputControl) srcControl.get();
		if (textField.getText() == null || textField.getText().isEmpty()) {
			return false;
		}

		return evalContent(textField.getText());
	}

	/**
	 * Handles validation for a <code>ComboBoxBase</code>.
	 *
	 * @return The validation result.
	 */
	private boolean evalComboBoxField() {
		ComboBoxBase<?> comboField = (ComboBoxBase<?>) srcControl.get();
		Object value = comboField.getValue();
		if (value == null || value.toString().isEmpty()) {
			return false;
		}

		return evalContent(value.toString());
	}

}
