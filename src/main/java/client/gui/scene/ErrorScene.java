package client.gui.scene;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * The <code>ErrorScene</code> class displays an exception inside of a JavaFX scene.
 */
public class ErrorScene {

	/**
	 * Create a <code>Scene</code> from the specified exception. Useful for displaying application
	 * breaking exceptions.
	 *
	 * @param exception Specifies what exception to represent
	 * @return The resulting scene.
	 */
	public static Scene fromException(Exception exception) {
		StringBuilder message = new StringBuilder(
			"Something went wrong! Please restart and contact the developers if the problem persists.\n\n");
		message.append(exception).append("\n");
		for (StackTraceElement traceElement : exception.getStackTrace()) {
			message.append("\tat ").append(traceElement).append("\n");
		}

		VBox box = new VBox();
		box.setPadding(new Insets(10));

		ScrollPane pane = new ScrollPane(box);
		pane.setMinSize(600, 400);
		pane.setMaxSize(800, 800);

		Text text = new Text();
		text.setFill(Color.RED);
		text.setText(message.toString());
		box.getChildren().add(text);
		return new Scene(pane);
	}
}
