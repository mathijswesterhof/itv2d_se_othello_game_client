package client.gui.scene;

import client.gui.controller.GameCounterController;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * The class <code>GameCounterScene</code> represents the base of any scene displayed in the
 * client.
 */
public class GameCounterScene extends Scene {

	protected GameCounterController controller;

	/**
	 * Construct a GameCounterScene from the specified parameters.
	 *
	 * @param root       Specifies the root of the scene.
	 * @param controller Specifies the controller that is attached to this scene.
	 */
	public GameCounterScene(Parent root, GameCounterController controller) {
		super(root);
		this.controller = controller;
		this.getStylesheets().add("css/default.css");
	}

	/**
	 * Return true if this scene has a controller, false otherwise.
	 *
	 * @return Weather or not this scene has a controller.
	 */
	public boolean hasController() {
		return controller != null;
	}

	/**
	 * Return the associated controller.
	 *
	 * @return The associated controller.
	 */
	public GameCounterController getController() {
		return controller;
	}
}
