package lang;

public class NoSuchResourceException extends RuntimeException {

	/**
	 * Constructs a {@code NoSuchResourceException} with no detail message.
	 */
	public NoSuchResourceException() {
		super();
	}
}
