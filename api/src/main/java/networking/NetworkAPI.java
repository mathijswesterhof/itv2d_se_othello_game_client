package networking;

import networking.authentication.v1.Authentication;
import networking.challenge.ChallengeStore;
import networking.game.GameStore;
import networking.lobby.LobbyStore;
import networking.player.PlayerStore;
import networking.protocol.MessageListener;

/**
 * The main class of the network API for the strategic game AI.
 */
public class NetworkAPI {

	private Authentication authentication;
	private Connection connection;
	private ChallengeStore challengeStore;
	private GameStore gameStore;
	private PlayerStore playerStore;
	private LobbyStore lobbyStore;

	/**
	 * Initializes the Authentication/GameStore/ChallengeStore/PlayerStore/LobbyStore objects.
	 */
	public NetworkAPI() {
		authentication = new Authentication(this);
		gameStore = new GameStore(this);
		challengeStore = new ChallengeStore(this);
		playerStore = new PlayerStore(this);
		lobbyStore = new LobbyStore(this);
	}

	/**
	 * This is the main method that creates a new instance of the NetworkAPI class.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		new NetworkAPI();
	}

	/**
	 * Set the connection variable to a new instance of the Connection class and open the
	 * connection.
	 *
	 * @param host the domain or ip address of the server
	 * @param port the port of the server
	 */
	public void openConnection(String host, int port) {
		connection = new Connection(host, port);
		connection.openConnection(this);
	}

	/**
	 * @return current Authentication object
	 */
	public Authentication getAuthentication() {
		return authentication;
	}

	/**
	 * @return current Connection object
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @return current ChallengeStore object
	 */
	public ChallengeStore getChallengeStore() {
		return challengeStore;
	}

	/**
	 * @return current GameStore object
	 */
	public GameStore getGameStore() {
		return gameStore;
	}

	/**
	 * @return current PlayerStore object
	 */
	public PlayerStore getPlayerStore() {
		return playerStore;
	}

	/**
	 * @return current LobbyStore object
	 */
	public LobbyStore getLobbyStore() {
		return lobbyStore;
	}

	/**
	 * Close the current connection to the server.
	 */
	public void close() {
		connection.closeConnection();
	}

	/**
	 * Add a MessageListener to the currently active MessageHandler.
	 *
	 * @param messageListener the MessageListener that needs to be added to the MessageHandler
	 */
	public void addMessageListener(MessageListener messageListener) {
		connection.getMessageHandler().addMessageListener(messageListener);
	}

	/**
	 * Check if a MessageListener has been added to the currently active MessageHandler.
	 *
	 * @param messageListener the MessageListener that will be checked for
	 * @return true if the MessageListener exists in the connection, otherwise false
	 */
	public boolean hasMessageListener(MessageListener messageListener) {
		return connection.getMessageHandler().hasMessageListener(messageListener);
	}
}
