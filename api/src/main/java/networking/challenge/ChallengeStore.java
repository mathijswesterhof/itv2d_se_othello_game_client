package networking.challenge;

import networking.NetworkAPI;
import networking.game.GameType;
import networking.player.Player;

public class ChallengeStore {

	private final NetworkAPI networkAPI;

	/**
	 * Class constructor.
	 *
	 * @param networkAPI the NetworkAPI that needs to be used
	 */
	public ChallengeStore(NetworkAPI networkAPI) {
		this.networkAPI = networkAPI;
	}

	/**
	 * Challenge a player to play a game of the given GameType.
	 *
	 * @param challenger the Player that will send the challenge
	 * @param opponent   the Player that will receive the challenge and be challenged
	 * @param gameType   the gametype of the game that will be played, given as a GameType
	 */
	public void challengePlayer(Player challenger, Player opponent, GameType gameType) {
		if (!challenger.getUsername().equals(opponent.getUsername())) {
			networkAPI.getConnection().getMessageHandler().regularRequest(
				String.format("challenge \"%s\" \"%s\"", opponent.getUsername(), gameType.getTag())
			);
		}
	}

	/**
	 * Accept a Challenge to play a game.
	 *
	 * @param challenge the Challenge that will be accepted
	 */
	public void acceptChallenge(Challenge challenge) {
		networkAPI.getConnection().getMessageHandler()
			.regularRequest("challenge accept " + challenge.getId());
		challenge.setAccepted(true);
	}

	/**
	 * Cancel a Challenge to play a game.
	 *
	 * @param challenge the Challenge that will be cancelled
	 */
	public void cancelChallenge(Challenge challenge) {
		challenge.setCancelled(true);
	}

}
