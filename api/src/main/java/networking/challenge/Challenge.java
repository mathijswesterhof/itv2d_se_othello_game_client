package networking.challenge;

import networking.player.Player;

public class Challenge {

	private Player challenger;
	private String gameType;
	private int id;
	private boolean cancelled;
	private boolean accepted;

	/**
	 * Class constructor.
	 *
	 * @param challenger the Player that initialized the Challenge
	 * @param gameType   the gametype of the Challenge, given as a String
	 * @param id         the id of the Challenge
	 */
	public Challenge(Player challenger, String gameType, int id) {
		this.challenger = challenger;
		this.gameType = gameType;
		this.id = id;
	}

	/**
	 * @return true if the Challenge has been accepted, otherwise false
	 */
	public boolean isAccepted() {
		return accepted;
	}

	/**
	 * Set if the challenge has been accepted.
	 *
	 * @param accepted whether the Challenge is accepted
	 */
	protected void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	/**
	 * @return true if the challenge is cancelled, otherwise false
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Set if the challenge has been cancelled
	 *
	 * @param cancelled whether the Challenge is cancelled
	 */
	protected void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	/**
	 * @return the Player that made the Challenge
	 */
	public Player getChallenger() {
		return challenger;
	}

	/**
	 * @return the gametype for the Challenge as a String
	 */
	public String getGameType() {
		return gameType;
	}

	/**
	 * @return the challenge's id as an int
	 */
	public int getId() {
		return id;
	}

}
