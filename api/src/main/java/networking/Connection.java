package networking;

import java.io.IOException;
import java.net.Socket;
import networking.protocol.MessageHandler;

public class Connection {

	private String serverHost;
	private int serverPort;
	private Socket socket;
	private MessageHandler messageHandler;
	private Thread messageHandlerThread;

	/**
	 * Class constructor. Assigns the given server host and port to the serverHost and serverPort
	 * variables.
	 *
	 * @param serverHost the ip address or domain of the server
	 * @param serverPort the port of the server
	 */
	public Connection(String serverHost, int serverPort) {
		this.serverHost = serverHost;
		this.serverPort = serverPort;
	}

	/**
	 * Try to open a new connection to the server.
	 *
	 * @param networkAPI instance of the NetworkAPI that needs to be used for the MessageHandler
	 */
	public void openConnection(NetworkAPI networkAPI) {
		try {
			socket = new Socket(this.serverHost, this.serverPort);
			messageHandler = new MessageHandler(socket, networkAPI);
			messageHandlerThread = new Thread(messageHandler, "Message Handler Thread");
			messageHandlerThread.start();
		} catch (IOException ex) {
			throw new RuntimeException("Something went wrong while connecting to the server", ex);
		}
	}

	/**
	 * Closes the connection to the server (if there is one).
	 */
	public void closeConnection() {
		if (socket != null && socket.isConnected()) {
			if (messageHandler.sendRequest("exit").equals("EXIT")) {
				forceClose();
			}
		}
	}

	/**
	 * Forces the connection to the server to be closed.
	 */
	private void forceClose() {
		if (socket == null) {
			return;
		}

		try {
			socket.close();
			messageHandlerThread.interrupt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		socket = null;
	}

	/**
	 * @return returns the current MessageHandler object
	 */
	public MessageHandler getMessageHandler() {
		return messageHandler;
	}

}
