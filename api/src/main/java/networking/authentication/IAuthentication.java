package networking.authentication;

import networking.player.Player;

public interface IAuthentication {

	void login(Player player) throws Exception;

	void logout(Player player);

	boolean validateUsername(String username);

	boolean checkNameAvailability(String username);

}
