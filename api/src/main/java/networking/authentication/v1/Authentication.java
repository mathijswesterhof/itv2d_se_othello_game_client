package networking.authentication.v1;

import networking.NetworkAPI;
import networking.authentication.IAuthentication;
import networking.lobby.Lobby;
import networking.player.Player;

public class Authentication implements IAuthentication {

	private final NetworkAPI networkAPI;

	/**
	 * @param networkAPI
	 */
	public Authentication(NetworkAPI networkAPI) {
		this.networkAPI = networkAPI;
	}

	/**
	 * Try to login a Player to the server.
	 *
	 * @param player the Player that needs to be logged in to the server
	 */
	public void login(Player player) {
		player.setAuthenticated(false);

		if (networkAPI.getConnection().getMessageHandler()
			.sendRequest("login " + player.getUsername()).equals("OK")) {
			player.setAuthenticated(true);
		}
	}

	/**
	 * Try to logout a Player from the server.
	 *
	 * @param player the Player that needs to be logged out from the server
	 */
	public void logout(Player player) {
		if (player == null) {
			return;
		}

		if (player.isAuthenticated()) {
			if (networkAPI.getConnection().getMessageHandler().sendRequest("logout")
				.equals("EXIT")) {
				player.setAuthenticated(false);
			}
		}
	}

	/**
	 * Check if the given username matches the rules for valid usernames.
	 *
	 * @param username the username that needs to be checked
	 * @return true if the given username is valid, otherwise false
	 */
	public boolean validateUsername(String username) {
		if (username == null) {
			return false;
		}

		if (username.isEmpty()) {
			return false;
		}

		if (username.length() > 64) {
			return false;
		}

		//Returns true for all printable ASCII characters
		return username.chars().allMatch((c) -> c > 31 && c < 128);
	}

	/**
	 * Check if the given username is available on the server.
	 *
	 * @param username the username that needs to be checked
	 * @return true if the given username is available on the server, otherwise false
	 */
	@Override
	public boolean checkNameAvailability(String username) {
		Lobby lobby = new Lobby();
		networkAPI.getLobbyStore().updatePlayers(lobby);
		for (Player player : lobby.getPlayers()) {
			if (username.equals(player.getUsername())) {
				return false;
			}
		}

		return true;
	}
}
