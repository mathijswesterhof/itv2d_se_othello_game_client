package networking.protocol;

import networking.challenge.Challenge;
import networking.game.Game;

public interface MessageListener {

	void onMatchFound(Game game);

	void onChallengeReceived(Challenge challenge);

	void onChallengeCanceled(int id);
}
