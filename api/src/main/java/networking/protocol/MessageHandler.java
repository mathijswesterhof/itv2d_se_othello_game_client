package networking.protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import networking.NetworkAPI;
import networking.challenge.Challenge;
import networking.game.Game;
import networking.game.GameResult;
import networking.game.GameType;
import networking.game.Move;
import networking.player.Player;
import networking.protocol.utilities.TextUtilities;

public class MessageHandler implements Runnable {

	public final BlockingQueue<String> socketBlockingQueue;
	private final SimpleDateFormat timestampFormat = new SimpleDateFormat("HH:mm:ss");
	private final ArrayList<MessageListener> messageListeners;
	private final NetworkAPI networkAPI;
	private Socket socket;
	private PrintWriter toServer;
	private BufferedReader userInput;
	private BufferedReader serverOutput;

	/**
	 * Class constructor.
	 *
	 * @param socket     the socket that should be used for reading and sending network messages
	 * @param networkAPI the NetworkAPI that needs to be used
	 * @throws IOException
	 */
	public MessageHandler(Socket socket, NetworkAPI networkAPI) throws IOException {
		this.socket = socket;
		this.messageListeners = new ArrayList<>();
		this.networkAPI = networkAPI;
		toServer = new PrintWriter(socket.getOutputStream(), true);
		userInput = new BufferedReader(new InputStreamReader(System.in));
		serverOutput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		socketBlockingQueue = new LinkedBlockingQueue<>();
	}


	/**
	 * Add a MessageListener to the currently active MessageHandler.
	 *
	 * @param messageListener the MessageListener that needs to be added to the MessageHandler
	 */
	public void addMessageListener(MessageListener messageListener) {
		messageListeners.add(messageListener);
	}

	/**
	 * Check if a MessageListener has been added to the currently active MessageHandler.
	 *
	 * @param messageListener the MessageListener that will be checked for
	 * @return true if the MessageListener exists in the connection, otherwise false
	 */
	public boolean hasMessageListener(MessageListener messageListener) {
		return messageListeners.contains(messageListener);
	}

	public void raiseOnMatchFound(Game game) {
		for (MessageListener messageListener : messageListeners) {
			messageListener.onMatchFound(game);
		}
	}

	public void raiseOnChallengeReceived(Challenge challenge) {
		for (MessageListener messageListener : messageListeners) {
			messageListener.onChallengeReceived(challenge);
		}
	}

	public void raiseOnChallengeCanceled(int id) {
		for (MessageListener messageListener : messageListeners) {
			messageListener.onChallengeCanceled(id);
		}
	}

	/**
	 * Send a plaintext message to the server.
	 *
	 * @param message the message that needs to be send
	 */
	public void sendMessage(String message) {
		try {
			System.out.println("[" + timestampFormat.format(new Date()) + "] OUT: " + message);
			toServer.println(message);
			toServer.flush();
		} catch (Exception ex) {
			throw new RuntimeException("Could not send message to server", ex);
		}
	}

	/**
	 * Send a plaintext request to the server and return the response.
	 *
	 * @param message the message that needs to be send
	 * @return the plaintext server response
	 */
	public String sendRequest(String message) {
		String serverResponse = "";

		try {
			Thread.sleep(100);
			socketBlockingQueue.clear();

			sendMessage(message);
			serverResponse = socketBlockingQueue.take();
		} catch (InterruptedException ignored) {
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return serverResponse;
	}

	/**
	 * Send a plaintext request to the server and check if its response is OK.
	 *
	 * @param message the message that needs to be send
	 * @return true if the servers response is OK, otherwise false
	 */
	public boolean regularRequest(String message) {
		if (sendRequest(message).equals("OK")) {
			try {
				String response = networkAPI.getConnection()
					.getMessageHandler().socketBlockingQueue.take();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Read the incoming server messages and take the appropriate actions.
	 */
	private void readServerMessages() {
		while (!Thread.currentThread().isInterrupted() && socket.isConnected()) {
			try {
				String input = serverOutput.readLine();
				System.out.println("[" + timestampFormat.format(new Date()) + "]  IN: " + input);
				if (input == null) {
					socketBlockingQueue.put("EXIT");
					return;
				}

				if (input.startsWith("SVR PLAYERLIST")) {
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAMELIST")) {
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME CHALLENGE CANCELLED")) {
					HashMap<String, String> data = TextUtilities.stringToHashMap(input);
					raiseOnChallengeCanceled(Integer.parseInt(data.get("CHALLENGENUMBER")));
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME CHALLENGE")) {
					HashMap<String, String> data = TextUtilities.stringToHashMap(input);
					Challenge challenge = new Challenge(
						new Player(data.get("CHALLENGER"), true),
						data.get("GAMETYPE"),
						Integer.parseInt(data.get("CHALLENGENUMBER"))
					);
					raiseOnChallengeReceived(challenge);
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME YOURTURN")) {
					networkAPI.getGameStore()
						.notifyTurn(networkAPI.getGameStore().getCurrentGame());
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME MOVE")) {
					HashMap<String, String> data = TextUtilities.stringToHashMap(input);
					Move move = new Move(
						new Player(data.get("PLAYER"), true),
						data.get("DETAILS"),
						Integer.parseInt(data.get("MOVE"))
					);

					networkAPI.getGameStore()
						.notifyMove(networkAPI.getGameStore().getCurrentGame(), move);
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME MATCH")) {
					HashMap<String, String> data = TextUtilities.stringToHashMap(input);
					boolean localTurn = data.get("PLAYERTOMOVE")
						.equals(networkAPI.getPlayerStore().getCurrentPlayer().getUsername());
					Game game = new Game(
						networkAPI.getPlayerStore().getCurrentPlayer(),
						new Player(data.get("OPPONENT"), true),
						GameType.fromTag(data.get("GAMETYPE")),
						localTurn
					);
					networkAPI.getGameStore().setCurrentGame(game);
					raiseOnMatchFound(game);
					socketBlockingQueue.put(input);
				} else if (input.startsWith("SVR GAME ")) {
					HashMap<String, String> data = TextUtilities.stringToHashMap(input);
					networkAPI.getGameStore().endGame(
						networkAPI.getGameStore().getCurrentGame(),
						GameResult.fromTag(input.substring(9, input.indexOf('{') - 1)),
						data.get("COMMENT"),
						Integer.parseInt(data.get("PLAYERONESCORE")),
						Integer.parseInt(data.get("PLAYERTWOSCORE"))
					);
					socketBlockingQueue.put(input);
				} else if (input.startsWith("ERR")) {
					socketBlockingQueue.put(input);
				} else if (input.startsWith("OK")) {
					socketBlockingQueue.put(input);
				}

			} catch (Exception ex) {
				throw new RuntimeException(
					"Something went wrong trying to read the server messages", ex);
			}
		}
	}

	/**
	 * @return the current socketBlockingQueue BlockingQueue.
	 */
	public BlockingQueue<String> getBlockingQueue() {
		return socketBlockingQueue;
	}

	/**
	 * Execute the readServerMessages() method once the Thread runs.
	 */
	@Override
	public void run() {
		readServerMessages();
	}

}
