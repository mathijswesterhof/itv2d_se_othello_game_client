package networking.protocol.utilities;

import java.util.HashMap;

public class TextUtilities {

	/**
	 * Convert the given String to a String[] array.
	 *
	 * @param inputString the String that needs to be converted
	 * @return a String[] array containing the input data
	 */
	public static String[] stringToArray(String inputString) {
		inputString = inputString.substring(inputString.indexOf("[") + 1, inputString.indexOf("]"));
		inputString = inputString.replace("\"", "");

		return inputString.split(", ");
	}

	/**
	 * Convert the given String to a HashMap containing String values and keys.
	 *
	 * @param inputString the String that needs to be converted
	 * @return a HashMap containing the input data
	 */
	public static HashMap<String, String> stringToHashMap(String inputString) {
		HashMap<String, String> hashMap = new HashMap<>();
		inputString = inputString.substring(inputString.indexOf("{") + 1, inputString.indexOf("}"));
		inputString = inputString.replace("\"", "");

		for (String pair : inputString.split(",")) {
			if (pair.split(": ").length < 2) {
				continue;
			}

			hashMap.put(pair.split(": ")[0].trim(), pair.split(": ")[1].trim());
		}

		return hashMap;
	}

}
