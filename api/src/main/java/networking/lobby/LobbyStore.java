package networking.lobby;

import java.util.ArrayList;
import networking.NetworkAPI;
import networking.game.GameType;
import networking.player.Player;
import networking.protocol.utilities.TextUtilities;

public class LobbyStore {

	private final NetworkAPI networkAPI;

	/**
	 * Class constructor.
	 *
	 * @param networkAPI the NetworkAPI that needs to be used
	 */
	public LobbyStore(NetworkAPI networkAPI) {
		this.networkAPI = networkAPI;
	}

	/**
	 * Update the list of players that are currently in the lobby.
	 *
	 * @param lobby the Lobby for which you want to update the list of players
	 */
	public void updatePlayers(Lobby lobby) {
		if (networkAPI.getConnection().getMessageHandler()
			.sendRequest("get playerlist").equals("OK")) {
			try {
				ArrayList<Player> players = new ArrayList<>();
				String[] response = TextUtilities.stringToArray(
					networkAPI.getConnection().getMessageHandler().socketBlockingQueue.take());
				for (String username : response) {
					players.add(new Player(username, true));
				}

				lobby.setPlayers(players.toArray(new Player[]{}));
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Update the list of games that are available to play on the server.
	 *
	 * @param lobby the Lobby for which you want to update the list of available games
	 */
	public void updateGameList(Lobby lobby) {
		if (networkAPI.getConnection().getMessageHandler().sendRequest("get gamelist")
			.equals("OK")) {
			try {
				String[] response = TextUtilities.stringToArray(
					networkAPI.getConnection().getMessageHandler().socketBlockingQueue.take());
				lobby.setGameTypes(GameType.fromTags(response));
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Subscribe to a given GameType.
	 *
	 * @param gameType the game that will be subscribed to
	 */
	public void subscribeToGame(GameType gameType) {
		networkAPI.getConnection().getMessageHandler()
			.sendRequest("subscribe " + gameType.getTag());
	}

	public void unsubscribeFromGame(GameType gameType) {
		// TODO: I think there is no cancel option but you can override this.
		// TODO: by sending another subscribe request (maybe the updated server does)
	}
}
