package networking.lobby;

import java.util.ArrayList;
import java.util.Arrays;
import networking.game.GameType;
import networking.player.Player;

public class Lobby {

	private Player[] players;
	private GameType[] gameTypes;

	/**
	 * @return an ArrayList containing all of the Players in the Lobby
	 */
	public ArrayList<Player> getPlayers() {
		return new ArrayList<>(Arrays.asList(players));
	}

	/**
	 * Set the Players that are in the Lobby using a Player array.
	 *
	 * @param players the Array of Player objects that will be used
	 */
	protected void setPlayers(Player[] players) {
		this.players = players;
	}

	/**
	 * @return a GameType Array containing the different GameTypes that are available in the Lobby
	 */
	public GameType[] getGameTypes() {
		return gameTypes;
	}

	/**
	 * Set the GameTypes in the Lobby using a GameType array.
	 *
	 * @param gameTypes the Array of GameType objects that will be used
	 */
	protected void setGameTypes(GameType[] gameTypes) {
		this.gameTypes = gameTypes;
	}
}
