package networking.player;

import networking.NetworkAPI;

public class PlayerStore {

	private final NetworkAPI networkAPI;
	private Player currentPlayer;

	/**
	 * Class constructor.
	 *
	 * @param networkAPI the NetworkAPI that needs to be used
	 */
	public PlayerStore(NetworkAPI networkAPI) {
		this.networkAPI = networkAPI;
	}

	/**
	 * @return the current Player object
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Set currentPlayer to the given Player object.
	 *
	 * @param currentPlayer the Player object that needs to be used
	 */
	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	/**
	 * Try to authenticate the player to the server.
	 *
	 * @param player the Player object that needs to be authenticated
	 * @return true if the player has been authenticated, otherwise false
	 */
	public boolean authorizePlayer(Player player) {
		try {
			this.networkAPI.getAuthentication().login(player);
		} catch (Exception e) {
			e.printStackTrace();
		}

		player.setAuthenticated(true);
		return player.isAuthenticated();
	}

	/**
	 * Try to log out a player from the server if authenticated.
	 *
	 * @param player the Player object that needs to be logged out
	 */
	public void logoutPlayer(Player player) {
		if (player == null) {
			return;
		}

		if (player.isAuthenticated()) {
			this.networkAPI.getAuthentication().logout(player);
			player.setAuthenticated(false);
		}
	}

	/**
	 * Check if the given username matches the rules for valid usernames.
	 *
	 * @param username the username that needs to be checked
	 * @return true if the given username is valid, otherwise false
	 */
	public boolean validateUsername(String username) {
		return this.networkAPI.getAuthentication().validateUsername(username);
	}

	/**
	 * Check if the given username is available on the server.
	 *
	 * @param username the username that needs to be checked
	 * @return true if the given username is available on the server, otherwise false
	 */
	public boolean checkUsernameAvailability(String username) {
		return this.networkAPI.getAuthentication().checkNameAvailability(username);
	}
}
