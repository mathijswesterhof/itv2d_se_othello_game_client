package networking.player;

public class Player {

	private String username;
	private boolean authenticated;
	private boolean remote;

	/**
	 * Class constructor.
	 *
	 * @param username the username of the Player
	 * @param remote   whether the Player is a remote player
	 */
	public Player(String username, boolean remote) {
		this.username = username;
		this.remote = remote;
	}

	/**
	 * @return true if the player is authenticated, otherwise false
	 */
	public boolean isAuthenticated() {
		return authenticated;
	}

	/**
	 * Set the authentication status for the player.
	 *
	 * @param authenticated whether the Player is authenticated
	 */
	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	/**
	 * @return whether the Player is remote
	 */
	public boolean isRemote() {
		return remote;
	}

	/**
	 * @return the username of the player
	 */
	public String getUsername() {
		return username;
	}

}
