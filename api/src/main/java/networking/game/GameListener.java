package networking.game;

import networking.game.event.GameEndEvent;
import networking.game.event.GameMoveEvent;
import networking.game.event.GameTurnEvent;

public interface GameListener {

	void onTurn(GameTurnEvent gameTurnEvent);

	void onEnd(GameEndEvent gameEndEvent);

	void onMove(GameMoveEvent gameMoveEvent);
}
