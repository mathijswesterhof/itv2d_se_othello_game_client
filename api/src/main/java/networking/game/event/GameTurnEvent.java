package networking.game.event;

import networking.game.Game;
import networking.game.GameListener;

/**
 * The <code>GameTurnEvent</code> class represents a request for a move from the local client.
 */
public class GameTurnEvent extends GameEvent {

	/**
	 * Construct a <code>GameMoveEvent</code> with the specified parameters.
	 *
	 * @param game Specifies what game this event belongs to.
	 */
	public GameTurnEvent(Game game) {
		super(game);
	}

	@Override
	protected void raise(GameListener gameListener) {
		gameListener.onTurn(this);
	}
}
