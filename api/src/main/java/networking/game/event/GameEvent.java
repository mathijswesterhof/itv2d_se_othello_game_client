package networking.game.event;

import networking.game.Game;
import networking.game.GameListener;

/**
 * The <code>GameEvent</code> class represents an arbitrary game event.
 */
public abstract class GameEvent {

	private Game game;

	/**
	 * Construct a game event of the specified game.
	 *
	 * @param game Specifies what game this event belongs to.
	 */
	public GameEvent(Game game) {
		this.game = game;
	}

	/**
	 * Return the associated game.
	 *
	 * @return The game this event originated from.
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Raises itself to the specified listener.
	 *
	 * @param gameListener Specifies the gameListener this event has to inform.
	 */
	public void raiseEvent(GameListener gameListener) {
		raise(gameListener);
	}

	/**
	 * Raises itself to the specified listener.
	 *
	 * @param gameListener Specifies the gameListener this event has to inform.
	 */
	protected abstract void raise(GameListener gameListener);
}
