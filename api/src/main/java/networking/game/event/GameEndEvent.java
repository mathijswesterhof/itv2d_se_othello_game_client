package networking.game.event;

import networking.game.Game;
import networking.game.GameListener;
import networking.game.GameResult;

/**
 * The <code>GameEndEvent</code> class represents the ending of a game.
 */
public class GameEndEvent extends GameEvent {

	private GameResult gameResult;
	private String endReason;
	private int playerOneScore;
	private int playerTwoScore;

	/**
	 * Construct a <code>GameEndEvent</code> with the specified parameters.
	 *
	 * @param game Specifies what game this event belongs to.
	 * @param gameResult Specifies the games result.
	 * @param endReason Specifies the games end reason.
	 * @param playerOneScore Specifies player one's total score.
	 * @param playerTwoScore Specifies player two's total score.
	 */
	public GameEndEvent(Game game, GameResult gameResult, String endReason, int playerOneScore,
		int playerTwoScore) {
		super(game);
		this.gameResult = gameResult;
		this.endReason = endReason;
		this.playerOneScore = playerOneScore;
		this.playerTwoScore = playerTwoScore;
	}

	/**
	 * Return the game result.
	 *
	 * @return The game result.
	 */
	public GameResult getGameResult() {
		return gameResult;
	}

	/**
	 * Return the end reason.
	 *
	 * @return The reason for the games end.
	 */
	public String getEndReason() {
		return endReason;
	}

	/**
	 * Return player one's total score.
	 *
	 * @return Player one's total score.
	 */
	public int getPlayerOneScore() {
		return playerOneScore;
	}

	/**
	 * Return player two's total score.
	 *
	 * @return Player two's total score.
	 */
	public int getPlayerTwoScore() {
		return playerTwoScore;
	}

	@Override
	protected void raise(GameListener gameListener) {
		gameListener.onEnd(this);
	}
}
