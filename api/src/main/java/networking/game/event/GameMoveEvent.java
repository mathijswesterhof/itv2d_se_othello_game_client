package networking.game.event;

import networking.game.Game;
import networking.game.GameListener;
import networking.game.Move;

/**
 * The <code>GameEndEvent</code> class represents an announced move from the server.
 */
public class GameMoveEvent extends GameEvent {

	private Move move;

	/**
	 * Construct a <code>GameMoveEvent</code> with the specified parameters.
	 *
	 * @param game Specifies what game this event belongs to.
	 * @param move Specifies what move has been made.
	 */
	public GameMoveEvent(Game game, Move move) {
		super(game);
		this.move = move;
	}

	/**
	 * Return the move with this associated event.
	 *
	 * @return The move this event represents.
	 */
	public Move getMove() {
		return move;
	}

	@Override
	protected void raise(GameListener gameListener) {
		gameListener.onMove(this);
	}
}
