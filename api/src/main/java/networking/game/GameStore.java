package networking.game;

import networking.NetworkAPI;
import networking.game.event.GameEndEvent;
import networking.game.event.GameMoveEvent;
import networking.game.event.GameTurnEvent;

public class GameStore {

	private final NetworkAPI networkAPI;
	private Game currentGame;

	public GameStore(NetworkAPI networkAPI) {
		this.networkAPI = networkAPI;
	}

	/**
	 * @return the current Game that is being played
	 */
	public Game getCurrentGame() {
		return this.currentGame;
	}

	/**
	 * Set the Game that is currently being played to the given Game object.
	 *
	 * @param currentGame the Game that is currently being played
	 */
	public void setCurrentGame(Game currentGame) {
		this.currentGame = currentGame;
	}

	/**
	 * Request move from the given Game object.
	 *
	 * @param game the Game object that needs to be used
	 */
	public void notifyTurn(Game game) { //TODO:: Implement TURNMESSAGE parameter
		game.setLocalTurn(true);
		game.raiseOnTurn(new GameTurnEvent(game));
	}


	/**
	 * Send a move to the server.
	 *
	 * @param game the Game that the move has been made in
	 * @param move the Move that has been made
	 * @return true if the Move is accepted by the server, otherwise false
	 */
	public boolean doMove(Game game, Move move) {
		if (!game.isLocalTurn() || game.hasEnded()) {
			return false;
		}

		if (networkAPI.getConnection().getMessageHandler().sendRequest("move " + move.getField())
			.equals("OK")) {
			game.setLocalTurn(false);
			return true;
		}
		return false;
	}

	/**
	 * Send a forfeit request for the given Game object to the server.
	 *
	 * @param game the Game object that needs to be used to forfeit
	 */
	public void forfeitGame(Game game) {
		if (game.hasEnded()) {
			return;
		}
		networkAPI.getConnection().getMessageHandler().sendRequest("forfeit");
	}

	/**
	 * Apply a Move to the given Game object.
	 *
	 * @param game the Game that needs to be used to apply the Move to
	 * @param move the Move that needs to be applied to the Game object
	 */
	public void notifyMove(Game game, Move move) {
		game.raiseOnMove(new GameMoveEvent(game, move));
	}

	/**
	 * End a game.
	 *
	 * @param game           the Game that needs to be ended
	 * @param gameResult     the result of the game
	 * @param endReason      the reason why the game was ended
	 * @param playerOneScore the score of player one
	 * @param playerTwoScore the score of player two
	 */
	public void endGame(Game game, GameResult gameResult, String endReason, int playerOneScore,
		int playerTwoScore) {
		GameEndEvent gameEndEvent = new GameEndEvent(game, gameResult, endReason, playerOneScore,
			playerTwoScore);
		game.endGame(gameEndEvent.getGameResult(), gameEndEvent.getEndReason(),
			gameEndEvent.getPlayerOneScore(), gameEndEvent.getPlayerTwoScore());
		game.raiseOnEnd(gameEndEvent);
	}
}
