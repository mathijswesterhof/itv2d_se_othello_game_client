package networking.game;

public enum GameResult {
	NONE("NONE", "none"),
	WIN("WIN", "won"),
	LOSS("LOSS", "lost"),
	DRAW("DRAW", "played draw");

	private String tag;
	private String result;

	GameResult(String tag, String result) {
		this.tag = tag;
		this.result = result;
	}

	/**
	 * Get a GameResult from a given tag
	 *
	 * @param tag the tag that needs to be converted to a GameResult
	 * @return a GameResult if the tag exists for one, otherwise null
	 */
	public static GameResult fromTag(String tag) {
		GameResult[] gameResults = GameResult.values();
		for (GameResult gameResult : gameResults) {
			if (gameResult.getTag().equals(tag)) {
				return gameResult;
			}
		}
		return NONE;
	}

	/**
	 * @return the tag of the GameResult
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * @return the result of the GameResult
	 */
	public String getResult() {
		return result;
	}
}
