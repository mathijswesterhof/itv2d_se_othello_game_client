package networking.game;

import java.util.LinkedList;
import java.util.Queue;
import networking.game.event.GameEndEvent;
import networking.game.event.GameEvent;
import networking.game.event.GameMoveEvent;
import networking.game.event.GameTurnEvent;
import networking.player.Player;

public class Game {

	private final Player localPlayer;
	private final Player remotePlayer;
	private final GameType gameType;
	private final Queue<GameEvent> gameEventQueue;
	private GameListener gameListener;
	private int playerOneScore;
	private int playerTwoScore;
	private boolean ended;
	private boolean localTurn;
	private GameResult gameResult;
	private String endReason;

	/**
	 * Class constructor.
	 *
	 * @param localPlayer  the Player object of the local player
	 * @param remotePlayer the Player object of the remote player
	 * @param gameType     the GameType of the game
	 * @param localTurn    whether a local turn is possible
	 */
	public Game(Player localPlayer, Player remotePlayer, GameType gameType, boolean localTurn) {
		this.localPlayer = localPlayer;
		this.remotePlayer = remotePlayer;
		this.gameEventQueue = new LinkedList<>();
		this.playerOneScore = 0;
		this.playerTwoScore = 0;
		this.ended = false;
		this.localTurn = localTurn;
		this.gameType = gameType;
		this.gameResult = GameResult.NONE;
		this.endReason = "";
	}

	/**
	 * Set the Game's GameListener.
	 *
	 * @param gameListener the GameListener that needs to be used
	 */
	public void setGameListener(GameListener gameListener) {
		this.gameListener = gameListener;
		while (!gameEventQueue.isEmpty()) {
			GameEvent gameEvent = gameEventQueue.poll();
			gameEvent.raiseEvent(gameListener);
		}
	}

	protected void raiseOnTurn(GameTurnEvent gameTurnEvent) {
		if (gameListener == null) {
			gameEventQueue.add(gameTurnEvent);
			return;
		}

		gameTurnEvent.raiseEvent(gameListener);
	}

	protected void raiseOnEnd(GameEndEvent gameEndEvent) {
		if (gameListener == null) {
			gameEventQueue.add(gameEndEvent);
			return;
		}

		gameEndEvent.raiseEvent(gameListener);
	}

	protected void raiseOnMove(GameMoveEvent gameMoveEvent) {
		if (gameListener == null) {
			gameEventQueue.add(gameMoveEvent);
			return;
		}

		gameMoveEvent.raiseEvent(gameListener);
	}

	/**
	 * @return true if a local turn is possible, otherwise false
	 */
	public boolean isLocalTurn() {
		return localTurn;
	}

	/**
	 * @param localTurn whether a local turn is possible
	 */
	protected void setLocalTurn(boolean localTurn) {
		this.localTurn = localTurn;
	}

	/**
	 * @return true if the game has ended, otherwise false
	 */
	public boolean hasEnded() {
		return ended;
	}

	/**
	 * @return the Player object for the local player
	 */
	public Player getLocalPlayer() {
		return localPlayer;
	}

	/**
	 * @return the Player object for the remote player
	 */
	public Player getRemotePlayer() {
		return remotePlayer;
	}

	/**
	 * @return the GameType of the Game
	 */
	public GameType getGameType() {
		return gameType;
	}

	/**
	 * End the Game and manage the results.
	 *
	 * @param gameResult     the result of the game
	 * @param endReason      the reason why the game was ended
	 * @param playerOneScore the score of player one
	 * @param playerTwoScore the scre of player two
	 */
	public void endGame(GameResult gameResult, String endReason, int playerOneScore,
		int playerTwoScore) {
		this.ended = true;
		this.gameResult = gameResult;
		this.endReason = endReason;
		this.playerOneScore = playerOneScore;
		this.playerTwoScore = playerTwoScore;
	}
}
