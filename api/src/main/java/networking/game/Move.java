package networking.game;

import networking.player.Player;

public class Move {

	private Player player;
	private String details;
	private int field;

	/**
	 * Class constructor.
	 *
	 * @param player  the Player that made the move
	 * @param details the details of the move
	 * @param field   the field of the move
	 */
	public Move(Player player, String details, int field) {
		this.player = player;
		this.details = details;
		this.field = field;
	}

	/**
	 * @return the Player that made the move
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Set the player that made the move.
	 *
	 * @param player the Player that will be used
	 */
	protected void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return a String containing the details of the move
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * Set the details of the move.
	 *
	 * @param details the details of the move.
	 */
	protected void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return an Integer containing the field on the board that the move was made to
	 */
	public int getField() {
		return field;
	}

	/**
	 * Set the field variable to the field on the board that the move was made to.
	 *
	 * @param move the field on the board that the move was made to
	 */
	protected void setField(String move) {
		this.field = field;
	}


}
