package networking.game;

import java.util.ArrayList;

public enum GameType {
	TIC_TAC_TOE("Tic-tac-toe"),
	REVERSI("Reversi");

	private String tag;

	GameType(String tag) {
		this.tag = tag;
	}

	/**
	 * Get a GameType from a given tag
	 *
	 * @param tag the tag that needs to be converted to a GameType
	 * @return a GameType if the tag exists for one, otherwise null
	 */
	public static GameType fromTag(String tag) {
		GameType[] gameTypes = GameType.values();
		for (GameType gameType : gameTypes) {
			if (gameType.getTag().equals(tag)) {
				return gameType;
			}
		}
		return null;
	}

	/**
	 * Get multiple GameType objects from a String[] Array of tags
	 *
	 * @param tags the tags that need to be converted to GameType objects
	 * @return an Array of GameType objects if the tags exist, otherwise null
	 */
	public static GameType[] fromTags(String[] tags) {
		ArrayList<GameType> resultList = new ArrayList<>();
		for (String tag : tags) {
			GameType gameType = fromTag(tag);
			if (gameType != null) {
				resultList.add(gameType);
			}
		}
		return resultList.toArray(new GameType[]{});
	}

	/**
	 * @return the tag of the GameType
	 */
	public String getTag() {
		return this.tag;
	}

	@Override
	public String toString() {
		return getTag();
	}
}
